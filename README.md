You need Netcore 3.0

Usage (all arguments are key-value pairs):  
&nbsp;&nbsp;&nbsp;-in: The input file  
&nbsp;&nbsp;&nbsp;-type: What type of asset is the input file  
&nbsp;&nbsp;&nbsp;-debug_level: The debug level (1=error,2=warning,3=info)  
&nbsp;&nbsp;&nbsp;-hash: String to hash  
&nbsp;&nbsp;&nbsp;-hashh: Hexadecimal string to inverse hash  
&nbsp;&nbsp;&nbsp;-section_filter: Which sections should be printed in unit decompilation  
  
## Formats Support
|                Format|Version|Support  |Remark       |
|:--------------------:|:-----:|:-------:|:-----------:|
|                Bundle|6?     |**Full** |             |
|                  Unit|185    |*Partial*|             |
|                 Level|180    |*Partial*|             |
|              Material|43     |Basic    |             |
|Shading<br>Environment|10     |**Full** |No Extraction|