using System.Collections.Generic;

namespace VTExtract {
	public static class ArrayExtension {
		public static void Swap<T>(this T[] array, int index1, int index2) {
			T temp = array[index1];
			array[index1] = array[index2];
			array[index2] = temp;
		}
	}
}