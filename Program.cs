﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Xml.Serialization;
using System.Text.Json;
using Collada141;
using System.Text.RegularExpressions;
using System.Text.Unicode;
using System.Globalization;
using System.Numerics;

namespace VTExtract {
	enum PrintModes {}

	class Program {
		public static Arguments? arguments {get; private set;}
		public static HashSet<string> filter {get; private set;} = new HashSet<string>();
		
		static void Main(string[] args) {
			arguments = new Arguments(args, "-in", "-out", "-print", "-type", "-debug_level", "-hash", "-hashh", "-section_filter");
			
			string? hash_input = arguments.GetArgumentValue("-hash");
			string? hashh_input = arguments.GetArgumentValue("-hashh");
			long? hashh = null;
			if(hashh_input != null) {
				hashh = long.Parse(hashh_input.PadRight(16,'0'), System.Globalization.NumberStyles.HexNumber);
			}
			string? input_path = arguments.GetArgumentValue("-in");
			string? output_path = arguments.GetArgumentValue("-out");
			string? print_mode = arguments.GetArgumentValue("-print")?.ToLower()?? "simple";
			string? asset_type = arguments.GetArgumentValue("-type")?.ToLower()?? "unit";
			string? debug_level = arguments.GetArgumentValue("-debug_level");
			if(debug_level != null) Debug.level = int.Parse(debug_level);
			string? section_filter = arguments.GetArgumentValue("-section_filter");
			if(section_filter != null) filter = section_filter.Split(',').ToHashSet();

			Stopwatch stopwatch = new Stopwatch();

			if(!string.IsNullOrWhiteSpace(input_path)) {
				var attributes = File.GetAttributes(input_path);
				if(attributes.HasFlag(FileAttributes.Directory)) {
					Debug.Print("Is Directory");
					throw new NotImplementedException();
				} else {
					switch(asset_type) {
						case "shading_environment":
							stopwatch.Start();
							Console.WriteLine("Opening File: " + Path.GetFileName(input_path));
							var sebytes = File.ReadAllBytes(input_path);

							var shading_environment = new ShadingEnvironment(Path.GetFileNameWithoutExtension(input_path));
							shading_environment.FromBytes(sebytes);

							//Console.WriteLine("Level Version: " + level.version);
							ExtractShadingEnvironment(shading_environment);
							break;
						case "material":
							stopwatch.Start();
							Console.WriteLine("Opening File: " + Path.GetFileName(input_path));
							var mbytes = File.ReadAllBytes(input_path);

							var material = new Material(Path.GetFileNameWithoutExtension(input_path));
							material.FromBytes(mbytes);

							//Console.WriteLine("Level Version: " + level.version);
							//ExtractLevel(material);
							break;
						case "level":
							stopwatch.Start();
							Console.WriteLine("Opening File: " + Path.GetFileName(input_path));
							var lbytes = File.ReadAllBytes(input_path);

							var level = new Level(Path.GetFileNameWithoutExtension(input_path));
							level.FromBytes(lbytes);

							//Console.WriteLine("Level Version: " + level.version);
							ExtractLevel(level);
							break;
						case "unit":
							stopwatch.Start();
							Console.WriteLine("Opening File: " + Path.GetFileName(input_path));
							var ubytes = File.ReadAllBytes(input_path);

							var unit = new UnitResource(Path.GetFileNameWithoutExtension(input_path));
							unit.FromBytes(ubytes);

							Console.WriteLine("Unit Version: " + unit.version);
							ExtractUnit(unit);
							break;
						case "bundle":
							var bundle = new Bundle();

							stopwatch.Start();
							bundle.FromBytesMT(input_path);
							bundle.DumpAllAssets();
							stopwatch.Stop();

							break;
						case "to_inverse":
								stopwatch.Start();
								Console.WriteLine("Opening File: " + Path.GetFileName(input_path));
								var titext = File.ReadAllText(input_path);
								TO_INVERSE(titext, output_path);
							break;
					}
				}
			}
			
			if(hash_input != null) {
				var hash = new MurmurHash2(hash_input);
				/* Console.WriteLine("input: " + hash_input);
				Console.WriteLine("            hasha: " + hasha.ToStringHash());
				Console.WriteLine("    inverse hasha: " + hasha.ToStringInverseHash()); */

				/* var baa = BitConverter.GetBytes(hasha.hash);
				Console.WriteLine(BitConverter.ToString(baa));

				var hash = new MurmurHash2((long)(int)(hasha.hash >> 32) << 32); */
				Console.WriteLine("input: " + hash_input);
				Console.WriteLine("             hash: " + hash.ToStringHash());
				Console.WriteLine("     inverse hash: " + hash.ToStringInverseHash());
				/* var bees = BitConverter.GetBytes(hash.inverseHash); */
				/* Console.WriteLine(BitConverter.ToString(bees));
				bees[0] = 0;
				bees[1] = 0;
				bees[2] = 0;
				bees[3] = 0;
				Console.WriteLine(BitConverter.ToString(bees)); */
				//Reverse so that bytes match ToStringInverseHash
				/* bees.Swap(0,7);
				bees.Swap(1,6);
				bees.Swap(2,5);
				bees.Swap(3,4);
				Console.WriteLine(BitConverter.ToString(bees)); */
				/* var str = Encoding.ASCII.GetString(bees.ToArray());
				var strbees = Encoding.ASCII.GetBytes(str);
				
				Console.WriteLine(str);
				Console.WriteLine(BitConverter.ToString(bees.ToArray()));
				Console.WriteLine(BitConverter.ToString(strbees)); */
				/* var hash2 = new MurmurHash2(bees);
				Console.WriteLine("            hash2: " + hash2.ToStringHash());
				Console.WriteLine("    inverse hash2: " + hash2.ToStringInverseHash()); */
			}

			if(hashh != null) {
				var hash = new MurmurHash2((long)hashh);
				Console.WriteLine("input: " + hashh_input);
				Console.WriteLine("             hash: " + hash.ToStringHash());
				Console.WriteLine("     inverse hash: " + hash.ToStringInverseHash());
			}
			Console.WriteLine("stopwatch.Elapsed: " + stopwatch.Elapsed);
		}
	
		static void RecursiveAddNodes(List<node> list, UnitResource.Node node, Dictionary<int, UnitResource.Renderable> renderables_by_name, List<List<UnitResource.Mesh.Material>> mesh_materials) {
			node n = new node();
			list.Add(n);
			var name = node.name.ToString("X8");
			n.id = name;
			n.name = name;
			n.type = node.isBone? NodeType.JOINT : NodeType.NODE;
			var lm = node.localPose;
			n.Items = new object[] {
				new matrix() {sid = "transform", Values = new double[] {lm.M11, lm.M21, lm.M31, lm.M41,
																						  lm.M12, lm.M22, lm.M32, lm.M42,
																						  lm.M13, lm.M23, lm.M33, lm.M43,
																						  lm.M14, lm.M24, lm.M34, lm.M44,}}
			};
			n.ItemsElementName = new ItemsChoiceType2[] {
				ItemsChoiceType2.matrix,
			};
			UnitResource.Renderable? renderable;
			if(renderables_by_name.TryGetValue(node.name, out renderable)) {
				var bind_material = new bind_material();
				var mesh_mats = mesh_materials[renderable.unknown_02 - 1];
				bind_material.technique_common = new instance_material[mesh_mats.Count];
				for(int i = 0; i < mesh_mats.Count; i++) {
					var instance_material = new instance_material();
					instance_material.symbol = mesh_mats[i].name.ToString("X8");
					instance_material.target = "#" + mesh_mats[i].name.ToString("X8");

					bind_material.technique_common[i] = instance_material;
				}
				string str = "mesh-" + (renderable.unknown_02-1); //name + "-mesh";
				n.instance_geometry = new instance_geometry[] {
					new instance_geometry() {url = "#" + str, name = str, bind_material = bind_material}
				};
				int skin_id = renderable.unknownSkinThing - renderables_by_name.Count;
				if(skin_id >= 0) {
					n.instance_controller = new instance_controller[] {
					new instance_controller() {url = "#skin-" + skin_id }
					};
				}
			}

			List<node> children = new List<node>();
			foreach(var no in node.children) {
				RecursiveAddNodes(children, no, renderables_by_name, mesh_materials);
			}
			n.node1 = children.ToArray();
		}

		static void ExtractUnit(UnitResource unit) {
			COLLADA model = new COLLADA();
			List<object> items = new List<object>();

			library_geometries geometries = new library_geometries();
			items.Add(geometries);
			library_controllers controllers = new library_controllers();
			items.Add(controllers);
			library_effects effects = new library_effects();
			items.Add(effects);
			library_materials materials = new library_materials();
			items.Add(materials);
			library_visual_scenes scenes = new library_visual_scenes();
			items.Add(scenes);

			scenes.visual_scene = new visual_scene[1];
			visual_scene scene = new visual_scene();
			scenes.visual_scene[0] = scene;

			scene.id = "Scene";
			scene.name = "Scene";

			List<material> mats = new List<material>();
			List<geometry> meshes = new List<geometry>();
			List<effect> list_effects = new List<effect>();
			for(int i = 0; i < unit.meshes.Count; i++) {
				var mesh = unit.meshes[i];
				var geo = new geometry();
				meshes.Add(geo);

				var name = "mesh-" + i; //unit.renderables[i].name.ToString("X8") + "-mesh";
				geo.name = name;
				geo.id = name;

				var m = new mesh();
				geo.Item = m;

				m.source = new source[mesh.sections.Count];
				for(int ii = 0; ii < mesh.sections.Count; ii++) {
					var source = new source();
					m.source[ii] = source;
					var section = mesh.sections[ii];

					source.id = name + "-" + section.elementType + "-" + section.UNKNOWN_sub_id;
					switch(section.elementType) {
						case UnitResource.Mesh.GeoSection.Type.COLOR:
							var item_c = new float_array();
							source.Item = item_c;
							item_c.count = (ulong)section.elementCount * 4;
							item_c.Values = new double[item_c.count];
							for(ulong iii = 0; iii < item_c.count; iii++) {
								item_c.Values[iii] = section.data[iii] / 255f;
							}
							source.technique_common = new sourceTechnique_common();
							var accessor_c = new accessor();
							source.technique_common.accessor = accessor_c;
							accessor_c.source = "#" + source.id;
							accessor_c.stride = 4;
							accessor_c.count = (ulong)section.elementCount;
							accessor_c.param = new param[] {
													new param() {name = "R", type = "float"},
													new param() {name = "G", type = "float"},
													new param() {name = "B", type = "float"},
													new param() {name = "A", type = "float"},
												};
							break;
						case UnitResource.Mesh.GeoSection.Type.POSITION:
							var item_p = new float_array();
							source.Item = item_p;
							item_p.count = (ulong)section.elementCount * 3;
							item_p.Values = new double[item_p.count];
							int offset_p = 0;
							for(int iii = 0; iii < (int)item_p.count; iii++) {
								var val = new HalfFloat(BitConverter.ToInt16(section.data, (iii + offset_p) * 2)).ToFloat();
								//Debug.Print(iii%3 + " | " + (iii+offset)*2 +" / "+ section.data.Length);
								item_p.Values[iii] = val;
								if(iii % 3 == 2) {
									offset_p++;
								}
							}
							source.technique_common = new sourceTechnique_common();
							var accessor_p = new accessor();
							source.technique_common.accessor = accessor_p;
							accessor_p.source = "#" + source.id;
							accessor_p.stride = 3;
							accessor_p.count = (ulong)section.elementCount;
							accessor_p.param = new param[] {
													new param() {name = "X", type = "float"},
													new param() {name = "Y", type = "float"},
													new param() {name = "Z", type = "float"},
												};
							break;
						case UnitResource.Mesh.GeoSection.Type.NORMAL:
							var item_n = new float_array();
							source.Item = item_n;
							item_n.count = (ulong)section.elementCount * 3;
							item_n.Values = new double[item_n.count];
							int offset_n = 0;
							for(int iii = 0; iii < (int)item_n.count; iii++) {
								var val = new HalfFloat(BitConverter.ToInt16(section.data, (iii + offset_n) * 2)).ToFloat();
								item_n.Values[iii] = val;
								if(iii % 3 == 2) {
									offset_n++;
								}
							}
							source.technique_common = new sourceTechnique_common();
							var accessor_n = new accessor();
							source.technique_common.accessor = accessor_n;
							accessor_n.source = "#" + source.id;
							accessor_n.stride = 3;
							accessor_n.count = (ulong)section.elementCount;
							accessor_n.param = new param[] {
													new param() {name = "X", type = "float"},
													new param() {name = "Y", type = "float"},
													new param() {name = "Z", type = "float"},
												};
							break;
						case UnitResource.Mesh.GeoSection.Type.TEXCOORD:
							var item_t = new float_array();
							source.Item = item_t;
							item_t.count = (ulong)section.elementCount * 2;
							item_t.Values = new double[item_t.count];
							for(int iii = 0; iii < (int)item_t.count; iii++) {
								item_t.Values[iii] = new HalfFloat(BitConverter.ToInt16(section.data, (int)iii * 2)).ToFloat();
							}
							source.technique_common = new sourceTechnique_common();
							var accessor_t = new accessor();
							source.technique_common.accessor = accessor_t;
							accessor_t.source = "#" + source.id;
							accessor_t.stride = 2;
							accessor_t.count = (ulong)section.elementCount;
							accessor_t.param = new param[] {
													new param() {name = "U", type = "float"},
													new param() {name = "V", type = "float"},
												};
							break;
						case UnitResource.Mesh.GeoSection.Type.UNKNOWN_SKIN2:
							var item_sv = new float_array();
							source.Item = item_sv;
							item_sv.count = (ulong)section.elementCount * 4;
							item_sv.Values = new double[item_sv.count];
							for(int iii = 0; iii < (int)item_sv.count; iii++) {
								item_sv.Values[iii] = new HalfFloat(BitConverter.ToInt16(section.data, (int)iii * 2)).ToFloat();
							}
							source.technique_common = new sourceTechnique_common();
							var accessor_sv = new accessor();
							source.technique_common.accessor = accessor_sv;
							accessor_sv.source = "#" + source.id;
							accessor_sv.stride = 4;
							accessor_sv.count = (ulong)section.elementCount;
							accessor_sv.param = new param[] {
													new param() {name = "WEIGHT1", type = "float"},
													new param() {name = "WEIGHT2", type = "float"},
													new param() {name = "WEIGHT3", type = "float"},
													new param() {name = "WEIGHT4", type = "float"},
												};
							break;
						case UnitResource.Mesh.GeoSection.Type.UNKNOWN_SKIN:
							var item_sb = new int_array();
							source.Item = item_sb;
							item_sb.count = (ulong)section.elementCount * 4;
							item_sb.Values = new int[item_sb.count];
							for(int iii = 0; iii < (int)item_sb.count; iii++) {
								item_sb.Values[iii] = section.data[iii];
							}
							source.technique_common = new sourceTechnique_common();
							var accessor_sb = new accessor();
							source.technique_common.accessor = accessor_sb;
							accessor_sb.source = "#" + source.id;
							accessor_sb.stride = 4;
							accessor_sb.count = (ulong)section.elementCount;
							accessor_sb.param = new param[] {
													new param() {name = "WEIGHT1", type = "integer"},
													new param() {name = "WEIGHT2", type = "integer"},
													new param() {name = "WEIGHT3", type = "integer"},
													new param() {name = "WEIGHT4", type = "integer"},
												};
							break;
					}
				}
				m.vertices = new vertices() {
					id = name + "-vertices",
					input = new InputLocal[] {
											new InputLocal() {
												semantic = "POSITION",
												source = "#" + name + "-POSITION-0",
											},
											new InputLocal() {
												semantic = "COLOR",
												source = "#" + name + "-COLOR-0",
											},
										}
				};
				/* foreach (var material in unit.)
				{

				} */
				m.Items = new object[mesh.materials.Count];
				for(int ii = 0; ii < mesh.materials.Count; ii++) {
					var mat = mesh.materials[ii];
					triangles tris = new triangles();
					m.Items[ii] = tris;
					tris.material = mat.name.ToString("X8");
					tris.count = (ulong)mat.triCount;
					StringBuilder str_builder = new StringBuilder();
					for(int iii = mat.triArrayStart * 3; iii < mat.triArrayStart * 3 + mat.triCount * 3; iii++) {
						str_builder.Append(mesh.triangles[iii] + " ");
					}
					tris.p = str_builder.ToString();
					tris.input = new InputLocalOffset[m.source.Length];
					for(int iii = 0; iii < m.source.Length; iii++) {
						tris.input[iii] = new InputLocalOffset();
						tris.input[iii].semantic = mesh.sections[iii].elementType.ToString();
						tris.input[iii].source = "#" + m.source[iii].id;
					}

					//TEMP
					Random random = new Random();
					var matt = new material();
					matt.id = mat.name.ToString("X8");
					matt.name = mat.name.ToString("X8");
					matt.instance_effect = new instance_effect();
					matt.instance_effect.url = "#" + matt.name + "-effect";
					if(!mats.Any(mat => mat.id == matt.id)) {
						var effect = new effect();
						effect.Items = new effectFx_profile_abstractProfile_COMMON[1];
						effect.id = matt.name + "-effect";
						var profile_common = effect.Items[0] = new effectFx_profile_abstractProfile_COMMON();
						var technique = profile_common.technique = new effectFx_profile_abstractProfile_COMMONTechnique();
						technique.sid = "common";
						var lambert = new effectFx_profile_abstractProfile_COMMONTechniqueLambert();
						technique.Item = lambert;
						lambert.diffuse = new common_color_or_texture_type();
						var col = new common_color_or_texture_typeColor();
						lambert.diffuse.Item = col;
						col.Values = new double[] { random.NextDouble(), random.NextDouble(), random.NextDouble(), 1 };

						list_effects.Add(effect);
						mats.Add(matt);
					}
				}
			}
			geometries.geometry = meshes.ToArray();
			materials.material = mats.ToArray();
			effects.effect = list_effects.ToArray();

			List<controller> skins_c = new List<controller>();
			foreach(var renderable in unit.renderables) {
				int skin_id = renderable.unknownSkinThing - unit.renderables.Count;
				if(skin_id >= 0) {
					var controller = new controller();
					skins_c.Add(controller);
					var s = new skin();
					controller.Item = s;

					var name = "skin-" + skin_id;
					controller.id = name;
					s.source1 = "#mesh-" + (renderable.unknown_02 - 1); //skin_id.ToString();
					s.joints = new skinJoints();
					s.joints.input = new InputLocal[] {
											new InputLocal() {semantic = "JOINT", source = "#" + name + "-joints"},
											new InputLocal() {semantic = "INV_BIND_MATRIX", source = "#" + name + "-inv_bmats"}
										};

					Name_array joints = new Name_array();
					s.source = new source[] {
											new source() {id = name + "-joints", Item = joints},
											new source() {id = name + "-inv_bmats"}
										};
				}
			}
			/* for(int i = 0; i < unit.skins.Count; i++) {
				var skin = unit.skins[i];
				var controller = new controller();
				skins_c.Add(controller);
				var s = new skin();
				controller.Item = s;
				s.
			} */
			controllers.controller = skins_c.ToArray();

			Dictionary<int, UnitResource.Renderable> renderables_by_name = unit.renderables.ToDictionary(r => r.name);

			List<node> nodes = new List<node>();
			List<List<UnitResource.Mesh.Material>> mesh_materials = new List<List<UnitResource.Mesh.Material>>();
			foreach(var mesh in unit.meshes) {
				mesh_materials.Add(mesh.materials);
			}
			var root = unit.nodes[0];
			RecursiveAddNodes(nodes, root, renderables_by_name, mesh_materials);
			scene.node = nodes.ToArray();

			model.scene = new COLLADAScene();
			model.scene.instance_visual_scene = new InstanceWithExtra();
			model.scene.instance_visual_scene.url = "#Scene";

			model.Items = items.ToArray();

			XmlSerializer serializer = new XmlSerializer(typeof(COLLADA));
			string path = string.Format("./{0}/", unit.name);
			Directory.CreateDirectory(path);
			using(FileStream stream = File.Open(Path.Combine(path, unit.name + ".dae"), FileMode.Create)) {
				serializer.Serialize(stream, model);
			}

			var unit_physics = new {
				actors = unit.actors,
				movers = unit.movers,
			};

			if(unit.actors.Count > 0) {
				var actors_json = JsonSerializer.Serialize(unit_physics, new JsonSerializerOptions() { WriteIndented = true });
				File.WriteAllText(Path.Combine(path, unit.name + ".physics"), actors_json);
			}

			/* for(int i = 0; i < unit.meshes.Count; i++) {
				var mesh = unit.meshes[i];
				for(int ii = 0; ii < mesh.sections.Count; ii++) {
					var section = mesh.sections[ii];
					var str = string.Format("{0}_{1}_{2}", unit.name, i, ii);
					File.WriteAllBytes(Path.Combine(path, str), section.data);
				}
			} */
		}
	
		static void ExtractLevel(Level level) {
			var options = new JsonSerializerOptions() { WriteIndented = true };
			options.Encoder = System.Text.Encodings.Web.JavaScriptEncoder.UnsafeRelaxedJsonEscaping;
			options.Converters.Add(new Vector3JsonConverter());
			options.Converters.Add(new QuaternionJsonConverter());
			options.Converters.Add(new MurmurHash2JsonConverter());

			var level_json = JsonSerializer.Serialize(level, options);
			File.WriteAllText(Path.Combine("./", level.name + ".level"), level_json);
		}

		static void ExtractShadingEnvironment(ShadingEnvironment shading_environment) {
			var options = new JsonSerializerOptions() { WriteIndented = true };
			options.Encoder = System.Text.Encodings.Web.JavaScriptEncoder.UnsafeRelaxedJsonEscaping;
			options.Converters.Add(new Vector2JsonConverter());
			options.Converters.Add(new Vector3JsonConverter());
			options.Converters.Add(new QuaternionJsonConverter());
			options.Converters.Add(new Matrix4x4JsonConverter());
			options.Converters.Add(new MurmurHash2JsonConverter());

			var se_file = new {
				settings = shading_environment.presets,
				template = shading_environment.name + ".shading_environment_template",
			};

			var st_file = new {
				variables = new Dictionary<string, ShadingEnvironment.TemplateVariable>(),
				material = shading_environment.material.ToStringHash(),
			};

			foreach(var setting in shading_environment.settings) {
				dynamic value;
				switch (setting.type) {
					case ShadingEnvironment.SettingType.scalar:
						value = 0f;
						break;
					case ShadingEnvironment.SettingType.vector2:
						value = new Vector2();
						break;
					case ShadingEnvironment.SettingType.vector3:
						value = new Vector3();
						break;
					case ShadingEnvironment.SettingType.vector4:
						value = new Quaternion();
						break;
					case ShadingEnvironment.SettingType.matrix4x4:
						value = new Matrix4x4();
						break;
					case ShadingEnvironment.SettingType.scalar_array:
						value = new float[0];
						break;
					case ShadingEnvironment.SettingType.vector2_array:
						value = new float[0];
						break;
					case ShadingEnvironment.SettingType.vector3_array:
						value = new float[0];
						break;
					case ShadingEnvironment.SettingType.vector4_array:
						value = new float[0];
						break;
					case ShadingEnvironment.SettingType.resource:
						value = "";
						break;
					case ShadingEnvironment.SettingType.integer:
						value = 0;
						break;
					default:
						value = 0;
						break;
				}
				
				var t_variable = new ShadingEnvironment.TemplateVariable();
				t_variable.type = setting.type.ToString();
				t_variable.value = value;
				st_file.variables.Add("TO_INVERSE:"+setting.setting_name.ToStringHash().Substring(0,8), t_variable);
			}

			var se_json = JsonSerializer.Serialize(se_file, options);
			var st_json = JsonSerializer.Serialize(st_file, options);
			//Let's not talk about this
			se_json = Regex.Replace(se_json, @"\[\s+([\-0-9\.]+,)\s+([\-0-9\.]+,*)\s+\]", @"[$1 $2]");
			se_json = Regex.Replace(se_json, @"\[\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,*)\s+\]", @"[$1 $2 $3]");
			se_json = Regex.Replace(se_json, @"\[\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,*)\s+\]", @"[$1 $2 $3 $4]");
			se_json = Regex.Replace(se_json, @"\[\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,*)\s+\]", @"[$1 $2 $3 $4 $5 $6 $7 $8 $9 $10 $11 $12 $13 $14 $15 $16]");

			//Do it again
			st_json = Regex.Replace(st_json, @"\[\s+([\-0-9\.]+,)\s+([\-0-9\.]+,*)\s+\]", @"[$1 $2]");
			st_json = Regex.Replace(st_json, @"\[\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,*)\s+\]", @"[$1 $2 $3]");
			st_json = Regex.Replace(st_json, @"\[\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,*)\s+\]", @"[$1 $2 $3 $4]");
			st_json = Regex.Replace(st_json, @"\[\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,)\s+([\-0-9\.]+,*)\s+\]", @"[$1 $2 $3 $4 $5 $6 $7 $8 $9 $10 $11 $12 $13 $14 $15 $16]");

			/* se_json = se_json.Replace(":{", ": {\n");
			se_json = se_json.Replace(":[", ": [\n"); */

			File.WriteAllText(Path.Combine("./", shading_environment.name + ".shading_environment_edit"), se_json);
			File.WriteAllText(Path.Combine("./", shading_environment.name + ".shading_environment_template_edit"), st_json);

			TO_INVERSE(se_json, shading_environment.name + ".shading_environment");
			TO_INVERSE(st_json, shading_environment.name + ".shading_environment_template");
			/* var fb = File.ReadAllBytes(Path.Combine("./", shading_environment.name + ".shading_environment_edit")).ToList();
			var matches = Regex.Matches(se_json, @"TO_INVERSE:(\w+)");
			foreach(Match match in matches.Reverse()) {
				var pos = match.Index;
				fb.RemoveRange(pos, 11);
				var str = Encoding.UTF8.GetString(fb.ToArray(), pos, 16);
				var hash = new MurmurHash2(long.Parse(str, System.Globalization.NumberStyles.HexNumber));
				var bytes = BitConverter.GetBytes(hash.inverseHash).ToList();
				for(int i = 0; i < bytes.Count; i++) {
					var b = bytes[i];
					if(b == '\\' || b == '"') {
						bytes.Insert(i, (byte)'\\');
					i++;
					}
				}
				fb.RemoveRange(pos, 16);
				fb.InsertRange(pos, bytes);
			}
			File.WriteAllBytes(Path.Combine("./", shading_environment.name + ".shading_environment"), fb.ToArray());
			
			var fbt = File.ReadAllBytes(Path.Combine("./", shading_environment.name + ".shading_environment_template_edit")).ToList();
			var matches_t = Regex.Matches(st_json, @"TO_INVERSE:(\w+)");
			foreach(Match match in matches_t.Reverse()) {
				var pos = match.Index;
				fbt.RemoveRange(pos, 11);
				var str = Encoding.UTF8.GetString(fbt.ToArray(), pos, 16);
				var hash = new MurmurHash2(long.Parse(str, System.Globalization.NumberStyles.HexNumber));
				var bytes = BitConverter.GetBytes(hash.inverseHash).ToList();
				for(int i = 0; i < bytes.Count; i++) {
					var b = bytes[i];
					if(b == '\\' || b == '"') {
						bytes.Insert(i, (byte)'\\');
					i++;
					}
				}
				fbt.RemoveRange(pos, 16);
				fbt.InsertRange(pos, bytes);
			}
			File.WriteAllBytes(Path.Combine("./", shading_environment.name + ".shading_environment_template"), fbt.ToArray()); */
		}

		static void TO_INVERSE(string json, string filename) {
			//var fb = File.ReadAllBytes(Path.Combine("./", shading_environment.name + ".shading_environment_edit")).ToList();
			var fb/* ufb */ = Encoding.UTF8.GetBytes(json).ToList();
			//var fb = Encoding.Convert(Encoding.UTF8, Encoding.Unicode, ufb).ToList();
			var matches = Regex.Matches(json, @"TO_INVERSE:(\w+)");
			//fb.InsertRange(0, new byte[]{0xFF,0xFE});
			foreach(Match match in matches.Reverse()) {
				var pos = match.Index; //var pos = match.Index*2+2;
				//Console.WriteLine(BitConverter.ToString(fb.GetRange(pos, 22).ToArray()));
				fb.RemoveRange(pos, 11); //fb.RemoveRange(pos, 22);
				var str_size = 0;
				while(fb[pos+str_size] != '\"') str_size++;
				var str = Encoding.UTF8.GetString(fb.ToArray(), pos, str_size);
				//Console.WriteLine("str: " + str);
				var hash = new MurmurHash2(long.Parse(str.PadRight(16,'0'), System.Globalization.NumberStyles.HexNumber));
				var bytes = BitConverter.GetBytes(hash.inverseHash).ToList();
				//Debug.Print("   str: " + str);
				//Debug.Print(" bytes: " + BitConverter.ToString(bytes.ToArray()));
				//Console.WriteLine(BitConverter.ToString(bytes.ToArray()));
				bytes.Swap(0,7);
				bytes.Swap(1,6);
				bytes.Swap(2,5);
				bytes.Swap(3,4);
				//Debug.Print("bytesi: " + BitConverter.ToString(bytes.ToArray()));
				//Console.WriteLine(BitConverter.ToString(bytes.ToArray()));
				for(int i = 0; i < bytes.Count; i++) {
					var b = bytes[i];
					if(b < 0x80) {
						if(b == '\\' || b == '"') {
							bytes.Insert(i, (byte)'\\');
							i++;
						}
					} else if(b < 0xC0) {
						bytes.Insert(i, 0xC2);
						i++;
					} else {
						bytes.Insert(i, 0xC3);
						i++;
						bytes[i] = (byte)(b - 0x40);
					}
					/* if(b == '\\' || b == '"') {
						bytes.Insert(i, (byte)'\\');
						i++;
					} */
				}
				fb.RemoveRange(pos, str_size);
				fb.InsertRange(pos, bytes);
			}
			File.WriteAllBytes(Path.Combine("./", filename), fb.ToArray());
		}
	}
}
