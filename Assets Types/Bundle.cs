using System.Collections.Generic;

namespace VTExtract {
	public partial class Bundle {
		public class Asset {
			/// <summary>
			/// The name of the asset (usually an hash)
			/// </summary>
			public long name; //TODO Replace with MurmurHash

			/// <summary>
			/// The extension of the asset (usually an hash)
			/// </summary>
			public long extension; //TODO Replace with MurmurHash

			/// <summary>
			/// The position of the asset in the uncompressed data
			/// </summary>
			public int position;

			/// <summary>
			/// The uncompressed size of the asset
			/// </summary>
			public int size;

			public byte[]? header;

			public string GetNameWithExtension() {
				return name.ToString("X16") + "." + extension.ToString("X16");
			}

			/* public override string ToString() {
				return TempHashDatabase.GetHashOrigin(name, name.ToString("X16")) + "." + TempHashDatabase.GetHashOrigin(extension, extension.ToString("X16"));
			} */
		}

		public int versionNumber { get; private set; }
		public int unpackedSize { get; private set; }
		public string? name { get; private set; }
		public string? extension { get; private set; }
		public string? filePath { get; private set; }

		public byte[]? unpackedData;
		List<Asset> assets = new List<Asset>();
	}
}