using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace VTExtract {
	public partial class ShadingEnvironment {
		public enum SettingType {scalar, vector2, vector3, vector4, matrix4x4, scalar_array, vector2_array, vector3_array, vector4_array, UNKNOWN9, resource, integer}
		public class SettingDefinition {
			public SettingType type { get; set; }
			public int array_size { get; set; }
			public MurmurHash2 setting_name { get; set; }
			public int position_within_preset { get; set; }
		}

		public class Preset {
			public Dictionary<string, float> variable_weights { get; set; } = new Dictionary<string, float>();
			public Dictionary<string, dynamic> variables { get; set; } = new Dictionary<string, dynamic>();
		}

		public class TemplateVariable {
			public string type { get; set; } //SettingType type { get; set; }
			public dynamic value { get; set; }
		}

		[JsonIgnore] public string name { get; private set; }
		[JsonIgnore] public int version { get; private set; }

		public MurmurHash2 material { get; private set; }
		public List<SettingDefinition> settings { get; set; } = new List<SettingDefinition>();
		public Dictionary<string, Preset> presets { get; set; } = new Dictionary<string, Preset>();
	}
}