
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace VTExtract {
	public partial class Material {
		[JsonIgnore] public string name { get; private set; }
		[JsonIgnore] public int version { get; private set; }

		[JsonPropertyName("parent_material")] public MurmurHash2 parentMaterial { get; private set; }
		[JsonPropertyName("textures")] public Dictionary<MurmurHash2, MurmurHash2> textures { get; private set; } = new Dictionary<MurmurHash2, MurmurHash2>();
		[JsonPropertyName("material_contexts")] public Dictionary<MurmurHash2, MurmurHash2> materialContexts { get; private set; } = new Dictionary<MurmurHash2, MurmurHash2>();
	}
}