using System;
using System.IO;

namespace VTExtract {
	public static class ConverterUtils {
		public static void ProcessSection(BinaryReader reader, string section_name, Action<BinaryReader> function) {
			var should_print = (Program.filter.Count == 0 || Program.filter.Contains(section_name));
			var old_debug_level = Debug.level;

			if(!should_print) Debug.level = 2;
			var spos = reader.BaseStream.Position;
			var slength = reader.BaseStream.Length;
			Debug.Print(string.Format("<{0}>: {1:X8}/{2:X8} = {3:P2}", section_name, spos, slength, (float)spos/slength));
			Debug.indent++;

			function.Invoke(reader);

			Debug.indent--;
			spos = reader.BaseStream.Position;
			slength = reader.BaseStream.Length;
			Debug.Print(string.Format("</{0}>: {1:X8}/{2:X8} = {3:P2}", section_name, spos, slength, (float)spos/slength));
			if(!should_print) Debug.level = old_debug_level;
		}

		public static void ProcessSectionAtAdress(BinaryReader reader, string section_name, Action<BinaryReader> function, int adress) {
			var og_offset = reader.BaseStream.Position;
			reader.BaseStream.Position = adress;
			ProcessSection(reader, section_name, function);
			reader.BaseStream.Position = og_offset;
		}
	}
}