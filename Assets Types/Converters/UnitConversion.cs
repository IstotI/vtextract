
using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using System.Text;

//Normals
//BiNormals
//Tangeant

namespace VTExtract {
	public partial class UnitResource:IConvertible {
		public UnitResource(string name) {
			this.name = name;
		}

		public byte[] ToBytes() {
			throw new System.NotImplementedException();
		}

		public void FromBytes(byte[] data) {
			using(var stream = new MemoryStream(data))
			using(var reader = new BinaryReader(stream)) {
				version = reader.ReadInt32Check(val => {return val == 185;}, "Unit Version is {0} when 185 is expected");

				//Debug.level = 2;
				//Debug.level = 3;
				ConverterUtils.ProcessSection(reader, "MeshGeometry", _ProcessSectionMeshGeometry); //mesh_geometries
				ConverterUtils.ProcessSection(reader, "Skins", _ProcessSectionSkins);  //skins //reader.ReadInt32Check(val => {return val == 0;}, "1 ReadInt32Check is {0} when 0 is expected: " + stream.Position.ToString("X8"));
				
				/* var unknown_count = reader.ReadInt32();
				Debug.Print("unknown_count: " + unknown_count);
				for(int i = 0; i < unknown_count; i++) {
					reader.ReadInt32();
				}
				Debug.Print(stream.Position.ToString("X8")); */
				var unknown_size = reader.ReadInt32(); //reader.ReadInt32Check(val => {return val == 0;}, "6 ReadInt32Check is {0} when 0 is expected: " + stream.Position.ToString("X8"));
				Debug.PrintWarning("unknown_size: " + unknown_size);
				reader.ReadBytes(unknown_size);

				/* var bs = reader.ReadInt32();//Check(val => {return val == 0;}, "2 ReadInt32Check is {0} when 0 is expected: " + stream.Position.ToString("X8")); //_simple_animation
				if(bs > 0) reader.ReadBytes(bs + 8); */
				var unknown_count = reader.ReadInt32Check(val => { return val == 0; }, "6 ReadInt32Check is {0} when 0 is expected: " + stream.Position.ToString("X8"));
				Debug.PrintWarning("unknown_count: " + unknown_count);
				reader.ReadBytes(unknown_count * 4);

				if(unknown_count > 0) {
					var unknown_count2 = reader.ReadInt32Check(val => { return val == 0; }, "7 ReadInt32Check is {0} when 0 is expected: " + stream.Position.ToString("X8"));
					Debug.PrintWarning("unknown_count2: " + unknown_count2);
					reader.ReadBytes(unknown_count2 * 4);
				}

				ConverterUtils.ProcessSection(reader, "Nodes", _ProcessSectionNodes); //_scene_graph
				ConverterUtils.ProcessSection(reader, "Renderables", _ProcessSectionRenderables); //meshes
				ConverterUtils.ProcessSection(reader, "Physics", _ProcessSectionPhysics); //actors //reader.ReadInt32Check(val => {return val == 0;}, "4 ReadInt32Check is {0} when 0 is expected: " + stream.Position.ToString("X8"));
				ConverterUtils.ProcessSection(reader, "Cameras", _ProcessSectionCameras); //cameras //reader.ReadInt32Check(val => {return val == 0;}, "5 ReadInt32Check is {0} when 0 is expected: " + stream.Position.ToString("X8"));
				ConverterUtils.ProcessSection(reader, "Lights", _ProcessSectionLights); //lights //reader.ReadInt32Check(val => {return val == 0;}, "6 ReadInt32Check is {0} when 0 is expected: " + stream.Position.ToString("X8"));
				ConverterUtils.ProcessSection(reader, "LOD", _ProcessSectionLOD); //lod_objects //reader.ReadInt32Check(val => {return val == 0;}, "7 ReadInt32Check is {0} when 0 is expected: " + stream.Position.ToString("X8"));
				reader.ReadInt32Check(val => {return val == 0;}, "8 ReadInt32Check is {0} when 0 is expected: " + stream.Position.ToString("X8")); //terrains
				reader.ReadInt32Check(val => {return val == 0;}, "9 ReadInt32Check is {0} when 0 is expected: " + stream.Position.ToString("X8")); //_joints
				ConverterUtils.ProcessSection(reader, "Movers", _ProcessSectionMovers); //_movers //reader.ReadInt32Check(val => {return val == 0;}, "10 ReadInt32Check is {0} when 0 is expected: " + stream.Position.ToString("X8"));
				reader.ReadInt32Check(val => {return val == 0;}, "11 ReadInt32Check is {0} when 0 is expected: " + stream.Position.ToString("X8")); //_script_events
				var UNKNOWN_has_animation_blender_bones = reader.ReadBoolean(); //_has_animation_blender_bones //reader.ReadInt32Check(val => {return val == 0;}, "12 ReadInt32Check is {0} when 0 is expected: " + stream.Position.ToString("X8"));
				var str_length = reader.ReadInt32(); //_animation_state_machine //reader.ReadInt32Check(val => {return val == 0;}, "13 ReadInt32Check is {0} when 0 is expected: " + stream.Position.ToString("X8"));
				var str = reader.ReadChars(str_length);
				Debug.Print("str: " + new string(str));
				ConverterUtils.ProcessSection(reader, "Script Data", _ProcessSectionScriptData); //_dynamic_data //reader.ReadInt32Check(val => {return val == 8;}, "14 ReadByteCheck is {0} when 8 is expected: " + stream.Position.ToString("X8"));
				ConverterUtils.ProcessSection(reader, "Visibility Groups", _ProcessSectionVisibilityGroups); //_visibility_groups//reader.ReadInt32Check(val => {return val == 0;}, "17 ReadInt32Check is {0} when 0 is expected: " + stream.Position.ToString("X8")); 
				
				var unknown_size2 = reader.ReadInt32(); //reader.ReadInt32Check(val => {return val == 0;}, "18 ReadInt32Check is {0} when 0 is expected: " + stream.Position.ToString("X8")); //_flow
				Debug.PrintWarning("unknown_size2: " + unknown_size2);
				reader.ReadBytes(unknown_size2);

				var unknown_size3 = reader.ReadInt32(); //reader.ReadInt32Check(val => {return val == 0;}, "19 ReadInt32Check is {0} when 0 is expected: " + stream.Position.ToString("X8")); //_flow_dynamic_data
				Debug.PrintWarning("unknown_size3: " + unknown_size3);
				reader.ReadBytes(unknown_size3);
				
				var d_size = reader.ReadInt32(); //Seems to be related to surface queries //_mesh_geometry_triangle_finder
				Debug.Print("d_size: " + d_size);
				reader.ReadBytes(d_size); //reader.ReadInt32Check(val => {return val == 0;}, "21 ReadInt32Check is {0} when 0 is expected: " + stream.Position.ToString("X8")); //_physics_scene_data
				
				reader.ReadInt32Check(val => {return val == 0;}, "22 ReadInt32Check is {0} when 0 is expected: " + stream.Position.ToString("X8")); //_physics_scene_data_64bit
				reader.ReadInt32Check(val => {return val == 0;}, "23 ReadInt32Check is {0} when 0 is expected: " + stream.Position.ToString("X8")); //_default_material_resource
				
				var peek = reader.ReadByte();
				stream.Position--;
				if(peek == 0x8C) { //No materials in .unit
					reader.ReadInt64Check(val => {return val == unchecked((long)0x8E02FAFB5F886D8C);}, "24 ReadInt64Check is {0} when 0x8E02FAFB5F886D8C is expected: " + stream.Position.ToString("X8"));
					reader.ReadInt32Check(val => {return val == 0;}, "25 ReadInt32Check is {0} when 0 is expected: " + stream.Position.ToString("X8"));
				} else {
					reader.ReadInt32Check(val => {return val == 0;}, "24 ReadInt32Check is {0} when 0 is expected: " + stream.Position.ToString("X8"));
					reader.ReadInt32Check(val => {return val == 0;}, "25 ReadInt32Check is {0} when 0 is expected: " + stream.Position.ToString("X8"));
					ConverterUtils.ProcessSection(reader, "MaterialResourceReferences", _ProcessSectionMaterialResourceReferences);
				}
				
				reader.ReadInt32Check(val => {return val == 0;}, "26 ReadInt32Check is {0} when 0 is expected: " + stream.Position.ToString("X8")); //_materials
				reader.ReadInt32Check(val => {return val == 0;}, "27 ReadInt32Check is {0} when 0 is expected: " + stream.Position.ToString("X8")); //_apex_data
				reader.ReadInt32Check(val => {return val == 0;}, "28 ReadInt32Check is {0} when 0 is expected: " + stream.Position.ToString("X8")); //vehicles
				reader.ReadInt32Check(val => {return val == 0;}, "29 ReadInt32Check is {0} when 0 is expected: " + stream.Position.ToString("X8")); //_skeleton_name
				
				Debug.Print(stream.Position.ToString("X8"));
				Debug.Print(stream.Length.ToString("X8"));
				Debug.Print((float)stream.Position/stream.Length*100+"%");
				//Debug.level = 2;

				foreach(var skin in skins) {
					foreach(var bone_id in skin.boneIds) {
						nodes[bone_id].isBone = true;
					}
				}
				
				#if false
				Console.WriteLine();

				var has_bones = reader.ReadInt32(); //Really?
				Console.WriteLine("has_bones: " + has_bones);

				if(has_bones == 1) {
					var bone_count = reader.ReadUInt32();
					Console.WriteLine("bone_count: " + bone_count);

					for(int bi = 0; bi < bone_count; bi++) {
						Console.WriteLine("  -bone-");
						var matrix = reader.ReadMatrix4x4();

						Console.WriteLine("  {0,6:F2} | {1,6:F2} | {2,6:F2} | {3,6:F2}", matrix.M11, matrix.M12, matrix.M13, matrix.M14);
						Console.WriteLine("  {0,6:F2} | {1,6:F2} | {2,6:F2} | {3,6:F2}", matrix.M21, matrix.M22, matrix.M23, matrix.M24);
						Console.WriteLine("  {0,6:F2} | {1,6:F2} | {2,6:F2} | {3,6:F2}", matrix.M31, matrix.M32, matrix.M33, matrix.M34);
						Console.WriteLine("  {0,6:F2} | {1,6:F2} | {2,6:F2} | {3,6:F2}", matrix.M41, matrix.M42, matrix.M43, matrix.M44);
					}
				} else if(has_bones != 0) {
					Debug.PrintWarning("has_bones has value: " + has_bones + " instead of 0 or 1");
				}

				Debug.Print(stream.Position.ToString("X8"));
				Debug.Print(stream.Length.ToString("X8"));
				Debug.Print((float)stream.Position/stream.Length*100+"%");
				Console.WriteLine();

				var bone_count2 = reader.ReadUInt32(); //bone_count again?
				Console.WriteLine("bone_count2: " + bone_count2);
				var unknown4 = reader.ReadUInt32(); //I'm guessing this is another section
				Console.WriteLine("unknown4: " + unknown4);

				var unknown_zero1 = reader.ReadUInt32(); //Probably physics section
				Console.WriteLine("unknown_zero1: " + unknown_zero1);
				var unknown_one = reader.ReadUInt32();
				Console.WriteLine("unknown_one: " + unknown_one);

				List<short> parents_one = new List<short>();
				List<short> parents = new List<short>();
				List<int> node_names = new List<int>();
				//var unknown_one16 = reader.ReadInt16();
				//Console.WriteLine("unknown_one16: " + unknown_one16);
				Console.WriteLine("node_count: " + node_count);
				parents_one.Add(1);
				parents.Add(-1);
				parents_one.Add(1);
				parents.Add(0);
				for(int ni = 0; ni < node_count-2; ni++) {
					parents_one.Add(reader.ReadInt16());
					parents.Add(reader.ReadInt16());
				}

				Debug.Print("REEEEEEEEE: " + stream.Position.ToString("X8"));
				Debug.Print(stream.Length.ToString("X8"));
				Debug.Print((float)stream.Position/stream.Length*100+"%");

				for(int ni = 0; ni < node_count; ni++) {
					node_names.Add(reader.ReadInt32());
				}

				for(int ni = 0; ni < node_count; ni++) {
					Console.WriteLine("{0}: {1:X8}, {2}", ni, node_names[ni], parents[ni]);
				}
				
				Debug.Print(stream.Position.ToString("X8"));
				Debug.Print(stream.Length.ToString("X8"));
				Debug.Print((float)stream.Position/stream.Length*100+"%");
				
				var UNKNOWN_M1 = reader.ReadInt32();
				Console.WriteLine("UNKNOWN_M1: " + UNKNOWN_M1);
				var one = reader.ReadByte();
				Console.WriteLine("one: " + one);

				var eight = reader.ReadInt32();
				Console.WriteLine("eight: " + eight);
				var FFFFFFFF = reader.ReadInt32();
				Console.WriteLine("FFFFFFFF: " + FFFFFFFF.ToString("X8"));
				
				Debug.Print(stream.Position.ToString("X8"));
				Debug.Print(stream.Length.ToString("X8"));
				Debug.Print((float)stream.Position/stream.Length*100+"%");
				#endif
			
			}
		}

		void _ProcessSectionMeshGeometry(BinaryReader reader) {
			meshes.Clear();
			var mesh_count = reader.ReadInt32();
			Debug.Print("mesh_count: " + mesh_count);
			Debug.Print("-Meshes-");
			Debug.indent++;
			for(int i = 0; i < mesh_count; i++) {
				var mesh = new Mesh();
				meshes.Add(mesh);

				Debug.Print("---");
				var section_count = reader.ReadInt32();
				Debug.Print("section_count: " + section_count);
				mesh.sections = new List<Mesh.GeoSection>();
				for(int si = 0; si < section_count; si++) {
					var section = new Mesh.GeoSection();
					mesh.sections.Add(section);

					var data_size = reader.ReadInt32();
					section.data = reader.ReadBytes(data_size);
					section.UNKNOWN_ZERO = reader.ReadInt32Check(val => {return val == 0;}, "section.UNKNOWN_ZERO is {0} when 0 is expected: " + reader.BaseStream.Position.ToString("X8"));
					section.UNKNOWN_ZERO2 = reader.ReadInt32Check(val => {return val == 0;}, "section.UNKNOWN_ZERO2 is {0} when 0 is expected: " + reader.BaseStream.Position.ToString("X8"));
					section.elementCount = reader.ReadInt32();
					section.elementSize = reader.ReadInt32();
				}

				var section_count2 = reader.ReadInt32();
				Debug.Print("section_count2: " + section_count2);
				for(int ii = 0; ii < section_count2; ii++) {
					var section = mesh.sections[ii];

					section.elementType = (Mesh.GeoSection.Type)reader.ReadInt32();
					section.UNKNOWN = reader.ReadInt32();
					section.UNKNOWN_sub_id = reader.ReadInt32();
					reader.ReadInt32Check(val => {return val == ii;}, "section id is {0} when "+ii+" is expected: " + reader.BaseStream.Position.ToString("X8"));
					section.UNKNOWN_ZERO_BYTE = reader.ReadByteCheck(val => {return val == 0;}, "section.UNKNOWN_ZERO_BYTE is {0} when 0 is expected: " + reader.BaseStream.Position.ToString("X8"));
				}

				var unknown_zero_1 = reader.ReadInt32();
				Debug.Print("unknown_zero_1: " + unknown_zero_1);
				var unknown_zero_2 = reader.ReadInt32();
				Debug.Print("unknown_zero_2: " + unknown_zero_2);
				var unknown_zero_3 = reader.ReadInt32();
				Debug.Print("unknown_zero_3: " + unknown_zero_3);
				var edge_count = reader.ReadInt32(); //Maybe
				Debug.Print("edge_count: " + edge_count);

				Debug.Print(reader.BaseStream.Position.ToString("X8"));
				Debug.Print(reader.BaseStream.Length.ToString("X8"));
				Debug.Print((float)reader.BaseStream.Position/reader.BaseStream.Length*100+"%");

				var tri_s_data_size = reader.ReadInt32();
				Debug.Print("tri_s_data_size: " + tri_s_data_size);
				mesh.triangles.Clear();
				if(unknown_zero_3 > 0) { //Number of verts above max value of short
					for(int ti = 0; ti < tri_s_data_size/4; ti++) {
						mesh.triangles.Add(reader.ReadInt32());
					}
				} else {
					for(int ti = 0; ti < tri_s_data_size/2; ti++) {
						mesh.triangles.Add(reader.ReadInt16());
					}
				}

				Debug.Print(reader.BaseStream.Position.ToString("X8"));
				Debug.Print(reader.BaseStream.Length.ToString("X8"));
				Debug.Print((float)reader.BaseStream.Position/reader.BaseStream.Length*100+"%");


				Debug.Print("-Sections-");
				Debug.indent++;
				for(int si = 0; si < mesh.sections.Count; si++) {
					var section = mesh.sections[si];

					Debug.Print("---");
					Debug.Print("section.data.Length: " + section.data.Length);
					Debug.Print("section.UNKNOWN_ZERO: " + section.UNKNOWN_ZERO);
					Debug.Print("section.UNKNOWN_ZERO2: " + section.UNKNOWN_ZERO2);
					Debug.Print("section.UNKNOWN_sub_id: " + section.UNKNOWN_sub_id);
					Debug.Print("section.element_count: " + section.elementCount);
					Debug.Print("section.element_size: " + section.elementSize);
					Debug.Print("section.element_type: " + section.elementType + " (" + (int)section.elementType + ")");
					Debug.Print("section.UNKNOWN: " + section.UNKNOWN);
				}
				Debug.indent--;

				Debug.Print("");
				var material_count = reader.ReadInt32();
				Debug.Print("material_count: " + material_count);
				Debug.Print("-Material-");
				Debug.indent++;
				for(int mi = 0; mi < material_count; mi++) {
					Mesh.Material material = new Mesh.Material();
					mesh.materials.Add(material);

					Debug.Print("---");
					material.id = reader.ReadInt32();
					Debug.Print("material.id: " + material.id);
					material.triArrayStart = reader.ReadInt32();
					Debug.Print("tri_array_start: " + material.triArrayStart);
					material.triCount = reader.ReadInt32();
					Debug.Print("tri_count: " + material.triCount);
					material.unknown_zero = reader.ReadInt32();
					Debug.Print("unknown_zero: " + material.unknown_zero);
				}
				Debug.indent--;

				var bounds = reader.ReadBounds();
				Debug.Print("bounds_lower_x: " + bounds.lx);
				Debug.Print("bounds_lower_y: " + bounds.ly);
				Debug.Print("bounds_lower_z: " + bounds.lz);
				Debug.Print("bounds_upper_x: " + bounds.ux);
				Debug.Print("bounds_upper_y: " + bounds.uy);
				Debug.Print("bounds_upper_z: " + bounds.uz);
				Debug.Print("bounds_UNKNOWN: " + bounds.UNKNOWN);

				var material_count2 = reader.ReadInt32();
				Debug.Print("material_count2: " + material_count2);
				for(int mi = 0; mi < material_count; mi++) {
					mesh.materials[mi].name = reader.ReadInt32();
					Debug.Print("  " + mesh.materials[mi].name.ToString("X8"));
				}
			}
			Debug.indent--;
		}
		
		void _ProcessSectionNodes(BinaryReader reader) {
			nodes.Clear();
			var node_count = reader.ReadInt32();
			Debug.Print("node_count: " + node_count);
			for(int ni = 0; ni < node_count; ni++) {
				nodes.Add(new Node());
			}
			Debug.Print("-Local-");
			Debug.indent++;
			for(int ni = 0; ni < node_count; ni++) {
				var node = nodes[ni];
				Debug.Print("---");
				node.localXAxis = reader.ReadVector3();
				node.localYAxis = reader.ReadVector3();
				node.localZAxis = reader.ReadVector3();
				node.localPosition = reader.ReadVector3();
				node.localScale = reader.ReadVector3();

				Debug.Print("X Axis Vector: " + node.localXAxis);
				Debug.Print("Y Axis Vector:" + node.localYAxis);
				Debug.Print("Z Axis Vector: " + node.localZAxis);
				Debug.Print("Position: " + node.localPosition);
				Debug.Print("Scale: " + node.localScale);
			}
			Debug.indent--;

			Debug.indent++;
			for(int ni = 0; ni < node_count; ni++) {
				var node = nodes[ni];
				//Debug.Print("---");
				node.worldPose = reader.ReadMatrix4x4();

				//Debug.Print(string.Format("{0,6:F2} | {1,6:F2} | {2,6:F2} | {3,6:F2}", matrix.M11, matrix.M12, matrix.M13, matrix.M14));
				//Debug.Print(string.Format("{0,6:F2} | {1,6:F2} | {2,6:F2} | {3,6:F2}", matrix.M21, matrix.M22, matrix.M23, matrix.M24));
				//Debug.Print(string.Format("{0,6:F2} | {1,6:F2} | {2,6:F2} | {3,6:F2}", matrix.M31, matrix.M32, matrix.M33, matrix.M34));
				//Debug.Print(string.Format("{0,6:F2} | {1,6:F2} | {2,6:F2} | {3,6:F2}", matrix.M41, matrix.M42, matrix.M43, matrix.M44));
			}
			Debug.indent--;
			
			reader.ReadInt32Check(val => {return val == 0;}, "[_ProcessSectionNodes] First ReadInt32Check is {0} when 0 is expected: " + reader.BaseStream.Position.ToString("X8"));
			reader.ReadInt32Check(val => {return val == 1;}, "[_ProcessSectionNodes] Second ReadInt32Check is {0} when 1 is expected: " + reader.BaseStream.Position.ToString("X8"));

			//@33D05F in EF20A4EBD4C7BCA5.E0A48D0BE9A7453F
			for(int ni = 0; ni < node_count-2; ni++) {
				var node = nodes[ni+2];

				node.UNKNOWN_one = reader.ReadInt16Check(val => {return val == 1;}, "[_ProcessSectionNodes] Third ReadInt32Check is {0} when 1 is expected: " + reader.BaseStream.Position.ToString("X8"));
				node.parentId = reader.ReadInt16Check(val => {return val >= 0 && val < node_count;}, "Parent id: {0} is outside expected range [0.."+node_count+"]: :" + reader.BaseStream.Position.ToString("X8"));
				nodes[node.parentId].children.Add(node);
			}
			nodes[0].children.Add(nodes[1]);

			for(int ni = 0; ni < node_count; ni++) {
				var node = nodes[ni];
				/*var parent_node = nodes[node.parent_id];
				var inv_wmatrix = new Matrix4x4();
				if(Matrix4x4.Invert(parent_node.world_matrix, out inv_wmatrix)) {
					node.local_matrix = node.world_matrix * inv_wmatrix;
				} else {
					Debug.PrintError("Failed to calculate local matrix", Debug.ErrorCode.INVALID_DATA);
				}*/
				node.localPose = new Matrix4x4(
					node.localXAxis.X, node.localXAxis.Y, node.localXAxis.Z, 0,
					node.localYAxis.X, node.localYAxis.Y, node.localYAxis.Z, 0,
					node.localZAxis.X, node.localZAxis.Y, node.localZAxis.Z, 0,
					node.localPosition.X, node.localPosition.Y, node.localPosition.Z, 1
				);

				node.name = reader.ReadInt32();
				//Debug.Print(ni + " node_name: " + node_name.ToString("X8"));
			}

			foreach(var node in nodes) {
				Debug.Print(string.Format("{0}: {1}, {2}", node.name.ToString("X8"), node.parentId, node.children.Count));
				Debug.indent++;
				Debug.Print("node.local_position: " + node.localPosition);
				Debug.Print("node.local_scale: " + node.localScale);
				Debug.Print("node.local_x_axis: " + node.localXAxis);
				Debug.Print("node.local_y_axis: " + node.localYAxis);
				Debug.Print("node.local_z_axis: " + node.localZAxis);

				Debug.Print("node.local_pose:");
				Debug.indent++;
				Debug.Print(string.Format("{0,6:F2} | {1,6:F2} | {2,6:F2} | {3,6:F2}", node.localPose.M11, node.localPose.M12, node.localPose.M13, node.localPose.M14));
				Debug.Print(string.Format("{0,6:F2} | {1,6:F2} | {2,6:F2} | {3,6:F2}", node.localPose.M21, node.localPose.M22, node.localPose.M23, node.localPose.M24));
				Debug.Print(string.Format("{0,6:F2} | {1,6:F2} | {2,6:F2} | {3,6:F2}", node.localPose.M31, node.localPose.M32, node.localPose.M33, node.localPose.M34));
				Debug.Print(string.Format("{0,6:F2} | {1,6:F2} | {2,6:F2} | {3,6:F2}", node.localPose.M41, node.localPose.M42, node.localPose.M43, node.localPose.M44));
				Debug.indent--;

				Debug.Print("node.world_pose:");
				Debug.indent++;
				Debug.Print(string.Format("{0,6:F2} | {1,6:F2} | {2,6:F2} | {3,6:F2}", node.worldPose.M11, node.worldPose.M12, node.worldPose.M13, node.worldPose.M14));
				Debug.Print(string.Format("{0,6:F2} | {1,6:F2} | {2,6:F2} | {3,6:F2}", node.worldPose.M21, node.worldPose.M22, node.worldPose.M23, node.worldPose.M24));
				Debug.Print(string.Format("{0,6:F2} | {1,6:F2} | {2,6:F2} | {3,6:F2}", node.worldPose.M31, node.worldPose.M32, node.worldPose.M33, node.worldPose.M34));
				Debug.Print(string.Format("{0,6:F2} | {1,6:F2} | {2,6:F2} | {3,6:F2}", node.worldPose.M41, node.worldPose.M42, node.worldPose.M43, node.worldPose.M44));
				Debug.indent--;
				Debug.indent--;
			}
		}

		void _ProcessSectionRenderables(BinaryReader reader) {
			renderables.Clear();
			var renderable_count = reader.ReadInt32();
			Debug.Print("renderable_count: " + renderable_count);
			Debug.Print("-Renderables-");
			Debug.indent++;
			for(int i = 0; i < renderable_count; i++) {
				var renderable = new Renderable();
				renderables.Add(renderable);

				Debug.Print("---");
				renderable.name = reader.ReadInt32();
				Debug.Print("renderable_name: " + renderable.name.ToString("X8"));
				renderable.nodeId = reader.ReadInt32();
				Debug.Print("node_id: " + renderable.nodeId);
				renderable.unknown_02 = reader.ReadInt32();
				Debug.Print("unknown_02: " + renderable.unknown_02);
				renderable.unknownSkinThing = reader.ReadInt32();
				Debug.Print("unknownSkinThing: " + renderable.unknownSkinThing);
				renderable.flags = reader.ReadInt32();
				Debug.Print("flags: " + Convert.ToString(renderable.flags, 2).PadLeft(32,'0'));
				renderable.bounds = reader.ReadBounds();
				Debug.Print("boundsLowerX: " + renderable.bounds.lx);
				Debug.Print("boundsLowerY: " + renderable.bounds.ly);
				Debug.Print("boundsLowerZ: " + renderable.bounds.lz);
				Debug.Print("boundsUpperX: " + renderable.bounds.ux);
				Debug.Print("boundsUpperY: " + renderable.bounds.uy);
				Debug.Print("boundsUpperZ: " + renderable.bounds.uz);
				Debug.Print("bounds_UNKNOWN: " + renderable.bounds.UNKNOWN);
			}
			Debug.indent--;
		}
	
		void _ProcessSectionMaterialResourceReferences(BinaryReader reader) {
			var material_count = reader.ReadInt32();
			Debug.Print("material_count: " + material_count);
			Debug.Print("-Materials-");
			Debug.indent++;
			for(int i = 0; i < material_count; i++) {
				Debug.Print("---");
				var material_name = reader.ReadInt32();
				Debug.Print("material_name: " + material_name.ToString("X8"));
				var resource_name = reader.ReadInt64();
				Debug.Print("resource_name: " + resource_name.ToString("X16"));
			}
			Debug.indent--;
		}

		void _ProcessSectionPhysics(BinaryReader reader) {
			actors.Clear();
			var actor_count = reader.ReadInt32();
			Debug.Print("actor_count: " + actor_count);
			for(int ai = 0; ai < actor_count; ai++) {
				Actor actor = new Actor();
				actors.Add(actor);

				actor.name = reader.ReadInt32();
				actor.template = reader.ReadInt32();
				actor.node = reader.ReadInt32();
				actor.mass = reader.ReadSingle();
				var shape_count = reader.ReadInt32();
				for(int si = 0; si < shape_count; si++) {
					Actor.Shape shape = new Actor.Shape();
					actor.shapes.Add(shape);
					shape.type = (Actor.Shape.Type)reader.ReadInt32();
					shape.material = reader.ReadInt32();
					shape.template = reader.ReadInt32();
					shape.pose = reader.ReadMatrix4x4();
					shape.physx_data_size = reader.ReadInt32(); //0 with Box and Capsule types
					shape.physx_data = reader.ReadBytes(shape.physx_data_size);
					shape.shape = reader.ReadInt32();
					switch(shape.type) {
						case Actor.Shape.Type.sphere: //Sphere
							var shape_sphere_radius = reader.ReadSingle();
							//Debug.Print("shape_box_half_size_x: " + shape_box_half_size_x);
							break;
						case Actor.Shape.Type.box: //Box
							var shape_box_half_size_x = reader.ReadSingle();
							//Debug.Print("shape_box_half_size_x: " + shape_box_half_size_x);
							var shape_box_half_size_y = reader.ReadSingle();
							//Debug.Print("shape_box_half_size_y: " + shape_box_half_size_y);
							var shape_box_half_size_z = reader.ReadSingle();
							//Debug.Print("shape_box_half_size_z: " + shape_box_half_size_z);
							break;
						case Actor.Shape.Type.capsule: //Capsule
							var shape_capsule_radius = reader.ReadSingle();
							//Debug.Print("shape_capsule_radius: " + shape_capsule_radius);
							var shape_capsule_length = reader.ReadSingle();
							//Debug.Print("shape_capsule_length: " + shape_capsule_length);
							break;
						case Actor.Shape.Type.mesh: //Mesh
							//Skip
							/* reader.ReadInt64Check(val => { return val == 0x4853454D0153584E; }, "NXS.MESH is {0} when 0x4D5856430153584E is expected: " + reader.BaseStream.Position.ToString("X8"));
							reader.ReadInt32Check(val => { return val == 15; }, "Mesh Check1 got {0} when 15 is expected: " + reader.BaseStream.Position.ToString("X8"));
							reader.ReadInt32Check(val => { return val == 1; }, "Mesh Check2 got {0} when 1 is expected: " + reader.BaseStream.Position.ToString("X8"));
							var unknown = reader.ReadInt32();
							Debug.Print("unknown: " + unknown);
							var vertex_count = reader.ReadInt32();
							Debug.Print("vertex_count: " + vertex_count);
							var triangle_count = reader.ReadInt32();
							Debug.Print("triangle_count: " + triangle_count);
							Debug.PrintWarning(reader.BaseStream.Position.ToString("X8"));
							for(int i = 0; i < vertex_count; i++) {
								reader.ReadVector3();
							}
							Debug.PrintWarning(reader.BaseStream.Position.ToString("X8"));
							var tris = reader.ReadBytes(triangle_count*4);
							Debug.PrintWarning(reader.BaseStream.Position.ToString("X8")); */
							break;
						case Actor.Shape.Type.convex: //Convex //https://community.bistudio.com/wiki/P3D_File_Format_-_ODOLV4x //https://web.archive.org/web/20191112065703/https://community.bistudio.com/wiki/P3D_File_Format_-_ODOLV4x#phsyx_data
							//Skip
							/* reader.ReadInt64Check(val => { return val == 0x4D5856430153584E; }, "NXS.CVXM is {0} when 0x4D5856430153584E is expected: " + reader.BaseStream.Position.ToString("X8"));
							reader.ReadInt64Check(val => { return val == 13; }, "NXS.CVXM 13 is {0} when 13 is expected: " + reader.BaseStream.Position.ToString("X8"));
							reader.ReadInt64Check(val => { return val == 0x4C484C4301454349; }, "ICE.CLHL is {0} when 0x4C484C4301454349 is expected: " + reader.BaseStream.Position.ToString("X8"));
							reader.ReadInt32Check(val => { return val == 8; }, "ICE.CLHL 8 is {0} when 8 is expected: " + reader.BaseStream.Position.ToString("X8"));
							reader.ReadInt64Check(val => { return val == 0x4C48564301454349; }, "ICE.CVHL is {0} when 0x4C48564301454349 is expected: " + reader.BaseStream.Position.ToString("X8"));
							reader.ReadInt32Check(val => { return val == 8; }, "ICE.CVHL 8 is {0} when 8 is expected: " + reader.BaseStream.Position.ToString("X8"));
							var vertex_count = reader.ReadInt32();
							Debug.Print("vertex_count: " + vertex_count);
							var triangle_count = reader.ReadInt32();
							Debug.Print("triangle_count: " + triangle_count);
							var face_count = reader.ReadInt32();
							Debug.Print("face_count: " + face_count);
							var triangle_count_x2 = reader.ReadInt32();
							Debug.Print("triangle_count_x2: " + triangle_count_x2);
							Debug.PrintWarning(reader.BaseStream.Position.ToString("X8"));
							for(int i = 0; i < vertex_count; i++) {
								reader.ReadVector3();
							}
							Debug.PrintWarning(reader.BaseStream.Position.ToString("X8"));
							reader.ReadBytes(face_count * 20-4);
							var un = reader.ReadInt16();
							var un2 = reader.ReadInt16();
							Debug.PrintWarning(reader.BaseStream.Position.ToString("X8"));
							Debug.PrintWarning(un);
							Debug.PrintWarning(triangle_count*3+un*2);
							var tris = reader.ReadBytes(triangle_count*3+un*2);//triangle_count_x2 * 3);
							Debug.PrintWarning(reader.BaseStream.Position.ToString("X8"));
							var bees = reader.ReadBytes(4);
							foreach(var b in bees) {
								Debug.PrintWarning(b.ToString("X"));
							} */
							break;
						default:
							Debug.PrintWarning("Got shape_type with unsupported value: " + shape.type + " at: " + reader.BaseStream.Position.ToString("X8"));
							break;
					}
				}
				actor.FFx24 = reader.ReadBytes(24);
				actor.enabled = reader.ReadBoolean();
			}

			Debug.Print("-Actors-");
			Debug.indent++;
			foreach(var actor in actors) {
				Debug.Print("---");
				Debug.Print("name: " + actor.name.ToString("X8"));
				Debug.Print("enabled: " + actor.enabled);
				Debug.Print("template: " + actor.template.ToString("X8"));
				Debug.Print("node: " + actor.node.ToString("X8"));
				Debug.Print("mass: " + actor.mass);

				Debug.Print("");
				Debug.Print("-Shapes-");
				Debug.indent++;
				foreach(var shape in actor.shapes) {
					Debug.Print("---");
					Debug.Print("type: " + shape.type + " (" + (int)shape.type + ")");
					Debug.Print("material: " + shape.material.ToString("X8"));
					Debug.Print("template: " + shape.template.ToString("X8"));
					Debug.Print("shape_pose:");
					Debug.indent++;
					Debug.Print(string.Format("{0,6:F2} | {1,6:F2} | {2,6:F2} | {3,6:F2}", shape.pose.M11, shape.pose.M12, shape.pose.M13, shape.pose.M14));
					Debug.Print(string.Format("{0,6:F2} | {1,6:F2} | {2,6:F2} | {3,6:F2}", shape.pose.M21, shape.pose.M22, shape.pose.M23, shape.pose.M24));
					Debug.Print(string.Format("{0,6:F2} | {1,6:F2} | {2,6:F2} | {3,6:F2}", shape.pose.M31, shape.pose.M32, shape.pose.M33, shape.pose.M34));
					Debug.Print(string.Format("{0,6:F2} | {1,6:F2} | {2,6:F2} | {3,6:F2}", shape.pose.M41, shape.pose.M42, shape.pose.M43, shape.pose.M44));
					Debug.indent--;
					Debug.Print("physx_data_size: " + shape.physx_data_size);
					Debug.Print("shape: " + shape.shape.ToString("X8"));
				}
				Debug.indent--;
			}
			Debug.indent--;
		}

		void _ProcessSectionMovers(BinaryReader reader) {
			movers.Clear();
			var mover_count = reader.ReadInt32();
			Debug.Print("mover_count: " + mover_count);
			for(int mi = 0; mi < mover_count; mi++) {
				Mover mover = new Mover();
				movers.Add(mover);

				mover.name = reader.ReadInt32();
				mover.height = reader.ReadSingle();
				mover.radius = reader.ReadSingle();
				mover.collisionFilter = reader.ReadInt32();
				mover.slope_limit = reader.ReadSingle(); //Radians?
				mover.FFx8 = reader.ReadBytes(8);
			}

			Debug.Print("-Movers-");
			Debug.indent++;
			foreach(var mover in movers) {
				Debug.Print("---");
				Debug.Print("mover_name: " + mover.name.ToString("X8"));
				Debug.Print("mover_height: " + mover.height);
				Debug.Print("mover_radius: " + mover.radius);
				Debug.Print("mover_collision_filter: " + mover.collisionFilter.ToString("X8"));
				Debug.Print("slope_limit: " + mover.slope_limit);
			}
			Debug.indent--;
		}

		void _ProcessSectionVisibilityGroups(BinaryReader reader) {
			var vg_count = reader.ReadInt32();
			Debug.Print("vg_count: " + vg_count);
			Debug.Print("-Visibility Groups-");
			Debug.indent++;
			for(int i = 0; i < vg_count; i++) {
				Debug.Print("---");
				var group_name = reader.ReadInt32();
				Debug.Print("group_name: " + group_name.ToString("X8"));
				var member_count = reader.ReadInt32();
				Debug.Print("member_count: " + member_count);
				Debug.Print("-Members-");
				Debug.indent++;
				for(int ii = 0; ii < member_count; ii++) {
					var renderable_id = reader.ReadInt32();
					Debug.Print("renderable_id: " + renderable_id);
				}
				Debug.indent--;
			}
			Debug.indent--;
		}

		void _ProcessSectionScriptData(BinaryReader reader) { //TODO
			var script_data_size = reader.ReadInt32();
			Debug.Print("script_data_size: " + script_data_size);
			reader.ReadBytes(script_data_size);
		}

		void _ProcessSectionLOD(BinaryReader reader) {
			var lob_object_count = reader.ReadInt32();
			Debug.Print("lob_object_count: " + lob_object_count);
			Debug.Print("-LOD Objects-");
			Debug.indent++;
			for(int i = 0; i < lob_object_count; i++) {
				Debug.Print("---");
				var name = reader.ReadInt32();
				Debug.Print("name: " + name.ToString("X8"));
				var orientation = reader.ReadInt32();
				Debug.Print("orientation_node_id: " + orientation);
				var step_count = reader.ReadInt32();
				Debug.Print("step_count: " + step_count);
				Debug.Print("-Steps-");
				Debug.indent++;
				for(int ii = 0; ii < step_count; ii++) {
					var step_end = reader.ReadSingle();
					Debug.Print("step_end: " + step_end);
					var step_start = reader.ReadSingle();
					Debug.Print("step_start: " + step_start);
					var renderable_count = reader.ReadInt32();
					Debug.Print("renderable_count: " + renderable_count);
					Debug.Print("-Renderables-");
					for(int iii = 0; iii < renderable_count; iii++) {
						var renderable_id = reader.ReadInt32();
						Debug.Print("renderable_id: " + renderable_id);
					}
				}
				Debug.indent--;
				var bounding_volume = reader.ReadBounds();
				Debug.Print("bounds_lower_x: " + bounding_volume.lx);
				Debug.Print("bounds_lower_y: " + bounding_volume.ly);
				Debug.Print("bounds_lower_z: " + bounding_volume.lz);
				Debug.Print("bounds_upper_x: " + bounding_volume.ux);
				Debug.Print("bounds_upper_y: " + bounding_volume.uy);
				Debug.Print("bounds_upper_z: " + bounding_volume.uz);
				Debug.Print("bounds_UNKNOWN: " + bounding_volume.UNKNOWN);
				var UNKNOWN_flags = reader.ReadInt32();
				Debug.Print("flags: " + Convert.ToString(UNKNOWN_flags, 2).PadLeft(32,'0'));
			}
			Debug.indent--;
		}

		void _ProcessSectionCameras(BinaryReader reader) {
			var camera_count = reader.ReadInt32();
			Debug.Print("camera_count: " + camera_count);
			Debug.Print("-Cameras-");
			Debug.indent++;
			for(int mi = 0; mi < camera_count; mi++) {
				Debug.Print("---");
				var name = reader.ReadInt32();
				Debug.Print("name: " + name.ToString("X8"));
				var UNKNOWN = reader.ReadInt32();
				Debug.Print("UNKNOWN: " + UNKNOWN);
				var near_range = reader.ReadSingle();
				Debug.Print("near_range: " + near_range);
				var far_range = reader.ReadSingle();
				Debug.Print("far_range: " + far_range);
				var vertical_fov = reader.ReadSingle();
				Debug.Print("vertical_fov: " + vertical_fov);
				var UNKNOWN2 = reader.ReadInt32();
				Debug.Print("UNKNOWN2: " + UNKNOWN2);
			}
			Debug.indent--;
		}

		void _ProcessSectionLights(BinaryReader reader) {
			lights.Clear();
			var light_count = reader.ReadInt32();
			Debug.Print("light_count: " + light_count);
			Debug.Print("-Lights-");
			Debug.indent++;
			for(int i = 0; i < light_count; i++) {
				Debug.Print("---");
				var name = reader.ReadInt32();
				Debug.Print("name: " + name.ToString("X8"));
				var node_id = reader.ReadInt32();
				Debug.Print("node_id: " + node_id);
				var color_r = reader.ReadSingle();
				Debug.Print("color_r: " + color_r);
				var color_g = reader.ReadSingle();
				Debug.Print("color_g: " + color_g);
				var color_b = reader.ReadSingle();
				Debug.Print("color_b: " + color_b);
				reader.ReadSingleCheck(val => { return val == 1; }, "_ProcessSectionLights 1: is {0} when 1 is expected: " + reader.BaseStream.Position.ToString("X8"));
				reader.ReadSingleCheck(val => { return val == 1; }, "_ProcessSectionLights 1: is {0} when 1 is expected: " + reader.BaseStream.Position.ToString("X8"));
				
				var va = reader.ReadSingle();
				var vb = reader.ReadSingle();
				var vc = reader.ReadSingle();
				var vd = reader.ReadSingle();
				var ve = reader.ReadSingle();
				var vf = reader.ReadSingle();

				var type = (Light.Type)reader.ReadInt32();
				switch(type) {
					case Light.Type.omni:
						Debug.Print("falloff_start: " + va);
						Debug.Print("falloff_end: " + vb);
						Debug.Print("UNKNOWN_66666666: " + vc);
						Debug.Print("spot_angle_start: " + vd);
						Debug.Print("spot_angle_end: " + ve);
						Debug.Print("UNKNOWN_66666666: " + vf);
						break;
					case Light.Type.spot:
						Debug.Print("falloff_start: " + va);
						Debug.Print("falloff_end: " + vb);
						Debug.Print("UNKNOWN_66666666: " + vc);
						Debug.Print("spot_angle_start: " + vd);
						Debug.Print("spot_angle_end: " + ve);
						Debug.Print("UNKNOWN_66666666: " + vf);
						break;
					case Light.Type.box:
						Debug.Print("box_min_x: " + va);
						Debug.Print("box_max_x: " + vb);
						Debug.Print("box_min_y: " + vc);
						Debug.Print("box_min_z: " + vd);
						Debug.Print("box_max_y: " + ve);
						Debug.Print("box_max_z: " + vf);
						break;
					case Light.Type.directional:
						Debug.Print("UNKNOWN: " + va);
						Debug.Print("UNKNOWN: " + vb);
						Debug.Print("UNKNOWN_66666666: " + vc);
						Debug.Print("UNKNOWN: " + vd);
						Debug.Print("UNKNOWN: " + ve);
						Debug.Print("UNKNOWN_66666666: " + vf);
						break;
				}

				Debug.Print("type: " + type);
				var cast_shadows = reader.ReadBoolean();
				Debug.Print("cast_shadows: " + cast_shadows);
				var bs = reader.ReadBytes(11);
				//Debug.Print(string.Format("bs: {0:X2} {1:X2} {2:X2} {3:X2} {4:X2} {5:X2} {6:X2} {7:X2} {8:X2} {9:X2} {10:X2}", bs[0], bs[1], bs[2], bs[3], bs[4], bs[5], bs[6], bs[7], bs[8], bs[9], bs[10]));
			}
			Debug.indent--;
		}

		void _ProcessSectionSkins(BinaryReader reader) {
			skins.Clear();
			var skin_count = reader.ReadInt32();
			Debug.Print("skin_count: " + skin_count);
			Debug.Print("-Skins-");
			Debug.indent++;
			for(int i = 0; i < skin_count; i++) {
				Skin skin = new Skin();
				skins.Add(skin);

				Debug.Print("---");
				var bone_count = reader.ReadInt32();
				Debug.Print("bone_count: " + bone_count);
				Debug.Print("-Bones (Bind Pose)-");
				Debug.indent++;
				for(int ii = 0; ii < bone_count; ii++) {
					Debug.Print("---");
					var matrix = reader.ReadMatrix4x4();
					Debug.Print("matrix:");
					Debug.indent++;
					Debug.Print(string.Format("{0,6:F2} | {1,6:F2} | {2,6:F2} | {3,6:F2}", matrix.M11, matrix.M12, matrix.M13, matrix.M14));
					Debug.Print(string.Format("{0,6:F2} | {1,6:F2} | {2,6:F2} | {3,6:F2}", matrix.M21, matrix.M22, matrix.M23, matrix.M24));
					Debug.Print(string.Format("{0,6:F2} | {1,6:F2} | {2,6:F2} | {3,6:F2}", matrix.M31, matrix.M32, matrix.M33, matrix.M34));
					Debug.Print(string.Format("{0,6:F2} | {1,6:F2} | {2,6:F2} | {3,6:F2}", matrix.M41, matrix.M42, matrix.M43, matrix.M44));
					Debug.indent--;
				}
				Debug.indent--;
				Debug.PrintWarning(reader.BaseStream.Position.ToString("X8"));

				var bone_count2 = reader.ReadInt32();
				Debug.Print("bone_count2: " + bone_count);
				HashSet<int> node_ids = new System.Collections.Generic.HashSet<int>(); //Armature bones ids
				Debug.indent++;
				for(int ii = 0; ii < bone_count2; ii++) {
					var nid = reader.ReadInt32();
					if(node_ids.Contains(nid)) {
						Debug.PrintWarning("nid: " + nid + " is already in the HashSet");
					} else {
						node_ids.Add(nid);
						skin.boneIds.Add(nid);
						Debug.Print("nid: " + nid);
					}
				}
				Debug.indent--;
				Debug.PrintWarning(reader.BaseStream.Position.ToString("X8"));
				
				var unknown = reader.ReadInt32();
				Debug.Print("unknown: " + unknown);
				for(int ii = 0; ii < unknown; ii++) {
					HashSet<int> node_ids2 = new System.Collections.Generic.HashSet<int>(); //GeoSection UNKNOWN_SKIN to Armature bone ids
					var unknown_vertex_group_count = reader.ReadInt32();
					Debug.Print("unknown_vertex_group_count: " + unknown_vertex_group_count);
					Debug.indent++;
					for(int iii = 0; iii < unknown_vertex_group_count; iii++) {
						var nid2 = reader.ReadInt32();
						if(node_ids2.Contains(nid2)) {
							Debug.PrintWarning("nid2: " + nid2 + " is already in the HashSet");
						}
						else {
							node_ids2.Add(nid2);
							Debug.Print("nid2: " + nid2);
						}
					}
				}

				//var unknown2 = reader.ReadInt32();
				//Debug.Print("unknown2: " + unknown2);
				/* var unknown = reader.ReadInt32();
				Debug.Print("unknown: " + unknown);
				var unknown_vertex_group_count = reader.ReadInt32();
				Debug.Print("unknown_vertex_group_count: " + unknown_vertex_group_count);
				var unknown_countt = reader.ReadInt32();
				Debug.Print("unknown_countt: " + unknown_countt);
				Debug.indent++;
				for(int ii = 0; ii < unknown_vertex_group_count; ii++) {
					Debug.Print("---");
					var uii = reader.ReadInt32();
					Debug.Print("uii: " + uii);
					//for(int iii = 0; iii < uii; iii++) {
					//	reader.ReadInt32();
					//}
				} */
				Debug.indent--;
				Debug.PrintWarning(reader.BaseStream.Position.ToString("X8"));
			}
			Debug.indent--;
		}
	}
}