
using System;
using System.IO;
using System.Numerics;

namespace VTExtract {
	public partial class ShadingEnvironment:IConvertible {
		public ShadingEnvironment(string name) {
			this.name = name;
		}

		public void FromBytes(byte[] data) {
			using(var stream = new MemoryStream(data))
			using(var reader = new BinaryReader(stream)) {
				version = reader.ReadInt32Check(val => { return val == 10; }, "Shading Environment Version is {0} when 10 is expected: " + stream.Position.ToString("X8"));

				#region PrintInfo
				Debug.Print("Name: " + name);
				Debug.Print("Version: " + version);
				#endregion

				material = new MurmurHash2(reader.ReadInt64());
				Debug.Print("material: " + material.ToStringHash());

				var setting_count = reader.ReadInt32();
				Debug.Print("setting_count:" + setting_count);
				Debug.Print("-Settings-");
				Debug.indent++;
				settings.Clear();
				for(int i = 0; i < setting_count; i++) {
					Debug.Print("---");
					var setting = new SettingDefinition();
					settings.Add(setting);
					setting.type = (SettingType)reader.ReadInt32();
					setting.array_size = reader.ReadInt32();
					setting.setting_name = new MurmurHash2(((long)reader.ReadInt32()) << 32);
					setting.position_within_preset = reader.ReadInt32();

					#region PrintInfo
					Debug.Print("type: " + setting.type);
					Debug.Print("array_size: " + setting.array_size);
					Debug.Print("setting_name: " + setting.setting_name.ToStringHash().Substring(0, 8));
					Debug.Print("position_within_preset: " + setting.position_within_preset);
					#endregion
				}
				Debug.indent--;

				var preset_count = reader.ReadInt32();
				Debug.Print("preset_count:" + preset_count);
				Debug.Print("-Presets-");
				Debug.indent++;
				for(int i = 0; i < preset_count; i++) {
					Debug.Print("---");
					var preset_name = new MurmurHash2(((long)reader.ReadInt32()) << 32);
					var data_size = reader.ReadInt32();

					var preset = new Preset();
					presets.Add("TO_INVERSE:"+preset_name.ToStringHash().Substring(0,8), preset);

					#region PrintInfo
					Debug.Print("preset_name: " + preset_name.ToStringHash().Substring(0, 8));
					Debug.Print("data_size: " + data_size);
					#endregion

					for(int ii = 0; ii < setting_count; ii++) {
						var setting = settings[ii];
						var sn = "TO_INVERSE:"+setting.setting_name.ToStringHash().Substring(0,8);
						switch(setting.type) {
							case SettingType.scalar:
								var single = reader.ReadSingle();
								Debug.Print(sn + " | scalar: " + single);
								preset.variables.Add(sn, single);
								break;
							case SettingType.vector2:
								var vector2 = reader.ReadVector2();
								Debug.Print(sn + " | vector2: " + vector2);
								preset.variables.Add(sn, vector2);
								break;
							case SettingType.vector3:
								var vector3 = reader.ReadVector3();
								Debug.Print(sn + " | vector3: " + vector3);
								preset.variables.Add(sn, vector3);
								break;
							case SettingType.vector4:
								var quaternion = reader.ReadQuaternion();
								Debug.Print(sn + " | vector4: " + quaternion);
								preset.variables.Add(sn, quaternion);
								break;
							case SettingType.matrix4x4:
								var m4x4 = reader.ReadMatrix4x4();
								Debug.Print(string.Format(sn + " | {0,6:F2} | {1,6:F2} | {2,6:F2} | {3,6:F2}", m4x4.M11, m4x4.M12, m4x4.M13, m4x4.M14));
								Debug.Print(string.Format("Matrix4x4| {0,6:F2} | {1,6:F2} | {2,6:F2} | {3,6:F2}", m4x4.M21, m4x4.M22, m4x4.M23, m4x4.M24));
								Debug.Print(string.Format("         | {0,6:F2} | {1,6:F2} | {2,6:F2} | {3,6:F2}", m4x4.M31, m4x4.M32, m4x4.M33, m4x4.M34));
								Debug.Print(string.Format("         | {0,6:F2} | {1,6:F2} | {2,6:F2} | {3,6:F2}", m4x4.M41, m4x4.M42, m4x4.M43, m4x4.M44));
								preset.variables.Add(sn, m4x4);
								break;
							case SettingType.scalar_array:
								float[] single_array = new float[setting.array_size];
								for(int iii = 0; iii < setting.array_size; iii++) {
									single_array[iii] = reader.ReadSingle();
								}
								Debug.Print(sn + " | scalar_array: [" + String.Join(", ", single_array) + "]");
								preset.variables.Add(sn, single_array);
								break;
							case SettingType.vector2_array:
								Vector2[] vector2_array = new Vector2[setting.array_size];
								for(int iii = 0; iii < setting.array_size; iii++) {
									vector2_array[iii] = reader.ReadVector2();
								}
								Debug.Print(sn + " | vector2_array: [" + String.Join(", ", vector2_array) + "]");
								preset.variables.Add(sn, vector2_array);
								break;
							case SettingType.vector3_array:
								Vector3[] vector3_array = new Vector3[setting.array_size];
								for(int iii = 0; iii < setting.array_size; iii++) {
									vector3_array[iii] = reader.ReadVector3();
								}
								Debug.Print(sn + " | vector3_array: [" + String.Join(", ", vector3_array) + "]");
								preset.variables.Add(sn, vector3_array);
								break;
							case SettingType.vector4_array:
								Quaternion[] quaternion_array = new Quaternion[setting.array_size];
								for(int iii = 0; iii < setting.array_size; iii++) {
									quaternion_array[iii] = reader.ReadQuaternion();
								}
								Debug.Print(sn + " | vector4_array: [" + String.Join(", ", quaternion_array) + "]");
								preset.variables.Add(sn, quaternion_array);
								break;
							case SettingType.resource:
								var resource = new MurmurHash2(reader.ReadInt64());
								var r_hash = resource.ToStringHash();
								Debug.Print(sn + " | resource: " + r_hash);
								reader.ReadInt32Check(val => { return val == -1; }, "FF FF FF FF is {0} when 0xFFFFFFFF is expected: " + stream.Position.ToString("X8"));
								reader.ReadInt32Check(val => { return val == 0x66666666; }, "66 66 66 66 is {0} when 0x66666666 is expected: " + stream.Position.ToString("X8"));
								preset.variables.Add(sn, r_hash == "0000000000000000" ? "" : r_hash); //TODO Maybe this can be done cleaner
								break;
							case SettingType.integer:
								var integer = (int)reader.ReadSingle();
								Debug.Print(sn + " | integer: " + integer);
								preset.variables.Add(sn, integer);
								break;
							default:
								Debug.PrintWarning("index: " + ii + " is of unsuported type: " + settings[ii].type + ": " + stream.Position.ToString("X8"));
								break;
						}
					}

					var weight_count = reader.ReadInt32();
					Debug.Print("weight_count:" + weight_count);
					Debug.Print("-Weights-");
					Debug.indent++;
					for(int ii = 0; ii < weight_count; ii++) {
						var setting_name = settings[ii].setting_name.ToStringHash().Substring(0, 8);
						var weight = reader.ReadSingle();
						Debug.Print(setting_name + " | " + weight);

						//TODO Make the if an option
						if(weight != 1) preset.variable_weights.Add(setting_name, weight);
					}
					Debug.indent--;
					Debug.Print(stream.Position.ToString("X8"));
					/* var type = reader.ReadInt32();
					var UNKNOWN_ONE = reader.ReadInt32Check(val => {return val == 1;}, "UNKNOWN_ONE is {0} when 1 is expected: " + stream.Position.ToString("X8"));
					var UNKNOWN = reader.ReadInt32();*/
				}
				Debug.indent--;

				reader.ReadInt32Check(val => { return val == 0; }, "Padding is {0} when 0 is expected: " + stream.Position.ToString("X8"));

				var setting_count2 = reader.ReadInt32();
				Debug.Print("setting_count2:" + setting_count2);
				if(setting_count2 != setting_count) Debug.PrintWarning("setting_count(" + setting_count + ") and setting_count2(" + setting_count2 + ") do not match");
				Debug.Print("-Settings II-");
				Debug.indent++;
				for(int i = 0; i < setting_count2; i++) {
					Debug.Print("---");
					var id = reader.ReadInt32();
					var wierd_id = reader.ReadInt32(); //Id within either of two groups (resource or everyting else)
					var type = (SettingType)reader.ReadInt32();
					var setting_name = new MurmurHash2(((long)reader.ReadInt32()) << 32);
					
					#region PrintInfo
					Debug.Print("id: " + id);
					Debug.Print("wierd_id: " + wierd_id);
					Debug.Print("type: " + type);
					Debug.Print("setting_name: " + setting_name.ToStringHash().Substring(0, 8));
					#endregion
				}
				Debug.indent--;
			}
		}

		public byte[] ToBytes() {
			throw new System.NotImplementedException();
		}
	}
}