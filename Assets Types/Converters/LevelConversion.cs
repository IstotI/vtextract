using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace VTExtract {
	public partial class Level: IConvertible {
		public Level(string name) {
			this.name = name;
		}

		public void FromBytes(byte[] data) {
			using(var stream = new MemoryStream(data))
			using(var reader = new BinaryReader(stream)) {
				version = reader.ReadInt32Check(val => {return val == 180;}, "Level Version is {0} when 180 is expected: " + stream.Position.ToString("X8"));

				#region PrintInfo
				Debug.Print("Name: " + name);
				Debug.Print("Version: " + version);
				#endregion

				unitCount = reader.ReadInt32();
				backgroundUnitCount = reader.ReadInt32();

				#region PrintInfo
				Debug.Print("unitCount: " + unitCount);
				Debug.Print("backgroundUnitCount: " + backgroundUnitCount);
				#endregion

				var section_adress_unit_data = reader.ReadInt32(); //Unit Data & Level Settings
				var section_adress_unknown = reader.ReadInt32();
				var section_adress_prototypes = reader.ReadInt32();
				var section_adress_prototypes_data_size = reader.ReadInt32(); //Or adress to prototypes collision
				var section_adress_unknown2 = reader.ReadInt32();
				var section_adress_unknown3 = reader.ReadInt32();
				var section_adress_unknown4_size = reader.ReadInt32();
				var section_adress_unknown5 = reader.ReadInt32();
				var section_adress_unknown6 = reader.ReadInt32();
				var section_adress_unknown7 = reader.ReadInt32();
				var section_adress_unknown8 = reader.ReadInt32();
				var section_adress_level_settings = reader.ReadInt32();
				var particles_count = reader.ReadInt32();
				var section_adress_particles = reader.ReadInt32();
				var section_adress_unknown12 = reader.ReadInt32();
				var section_adress_unknown13 = reader.ReadInt32();
				var section_adress_unknown14 = reader.ReadInt32();
				var section_adress_unknown15 = reader.ReadInt32();
				var section_adress_unknown16 = reader.ReadInt32();
				var section_adress_unknown17 = reader.ReadInt32();
				var section_adress_unknown18 = reader.ReadInt32();
				var section_adress_unknown19 = reader.ReadInt32();
				var section_adress_unknown20 = reader.ReadInt32();
				var section_adress_unknown21 = reader.ReadInt32();
				var section_adress_unknown22 = reader.ReadInt32();
				var section_adress_stories = reader.ReadInt32();
				var section_adress_unknown24 = reader.ReadInt32();
				var section_adress_unknown25 = reader.ReadInt32();
				var section_adress_unknown26 = reader.ReadInt32();
				var section_adress_unknown27 = reader.ReadInt32();
				var section_adress_unknown28 = reader.ReadInt32();
				var section_adress_unknown29 = reader.ReadInt32();
				var section_adress_unit_materials = reader.ReadInt32();
				var section_adress_unknown31 = reader.ReadInt32();
				var section_adress_unknown32 = reader.ReadInt32();
				var section_adress_unknown33 = reader.ReadInt32();
				var section_adress_unknown34 = reader.ReadInt32();
				
				Debug.Print("section_adress_unit_data: " + section_adress_unit_data);
				Debug.Print("section_adress_unknown: " + section_adress_unknown);
				Debug.Print("section_adress_prototypes: " + section_adress_prototypes);
				Debug.Print("section_adress_prototypes_data_size: " + section_adress_prototypes_data_size);
				Debug.Print("section_adress_flow: " + section_adress_unknown2);
				Debug.Print("section_adress_unknown3: " + section_adress_unknown3);
				Debug.Print("section_adress_unknown4_size: " + section_adress_unknown4_size);
				Debug.Print("section_adress_unknown5: " + section_adress_unknown5);
				Debug.Print("section_adress_unknown6: " + section_adress_unknown6);
				Debug.Print("section_adress_unknown7: " + section_adress_unknown7);
				Debug.Print("section_adress_unknown8: " + section_adress_unknown8);
				Debug.Print("section_adress_level_settings: " + section_adress_level_settings);
				Debug.Print("particles_count: " + particles_count);
				Debug.Print("section_adress_particles: " + section_adress_particles);
				Debug.Print("section_adress_unknown12: " + section_adress_unknown12);
				Debug.Print("section_adress_unknown13: " + section_adress_unknown13);
				Debug.Print("section_adress_unknown14: " + section_adress_unknown14);
				Debug.Print("section_adress_unknown15: " + section_adress_unknown15);
				Debug.Print("section_adress_unknown16: " + section_adress_unknown16);
				Debug.Print("section_adress_unknown17: " + section_adress_unknown17);
				Debug.Print("section_adress_unknown18: " + section_adress_unknown18);
				Debug.Print("section_adress_unknown19: " + section_adress_unknown19);
				Debug.Print("section_adress_unknown20: " + section_adress_unknown20);
				Debug.Print("section_adress_unknown21: " + section_adress_unknown21);
				Debug.Print("section_adress_unknown22: " + section_adress_unknown22);
				Debug.Print("section_adress_unknown23: " + section_adress_stories);
				Debug.Print("section_adress_unknown24: " + section_adress_unknown24);
				Debug.Print("section_adress_unknown25: " + section_adress_unknown25);
				Debug.Print("section_adress_unknown26: " + section_adress_unknown26);
				Debug.Print("section_adress_unknown27: " + section_adress_unknown27);
				Debug.Print("section_adress_unknown28: " + section_adress_unknown28);
				Debug.Print("section_adress_unknown29: " + section_adress_unknown29);
				Debug.Print("section_adress_unit_materials: " + section_adress_unit_materials);
				Debug.Print("section_adress_unknown31: " + section_adress_unknown31);
				Debug.Print("section_adress_unknown32: " + section_adress_unknown32);
				Debug.Print("section_adress_unknown33: " + section_adress_unknown33);
				Debug.Print("section_adress_unknown34: " + section_adress_unknown34);

				Debug.Print("-Level Settings-");
				ReadSJSONData(reader, section_adress_level_settings, levelSettings);

				var og_offset = reader.BaseStream.Position;
				reader.BaseStream.Position = section_adress_unit_materials;
				Debug.Print("-Unit Materials-");
				var mat_array_count = reader.ReadInt32();
				Debug.Print("mat_array_count: " + mat_array_count);
				Debug.indent++;
				for(int i = 0; i < mat_array_count; i++) {
					Debug.Print("---");
					var unit_id = reader.ReadInt32();
					var mat_count = reader.ReadInt32();

					Debug.Print("unit_id: " + unit_id);
					Debug.Print("mat_count: " + mat_count);
					Debug.indent++;
					for(int ii = 0; ii < mat_count; ii++) {
						var mat_name = new MurmurHash2(((long)reader.ReadInt32())<<32);
						var mat_resource = new MurmurHash2(reader.ReadInt64());
						Debug.Print(mat_name.ToStringHash().Substring(0,8) + " = " + mat_resource.ToStringHash());
					}
					Debug.indent--;
				}
				Debug.indent--;
				reader.BaseStream.Position = og_offset;
				
				/* og_offset = reader.BaseStream.Position;
				reader.BaseStream.Position = section_adress_stories;
				Debug.Print("-Stories-");
				var story_count = reader.ReadInt32();
				Debug.Print("story_count: " + story_count);
				Debug.indent++;
				List<int> story_offsets = new List<int>();
				for(int i = 0; i < story_count; i++) {
					Debug.Print("---");
					var story_name = new MurmurHash2(((long)reader.ReadInt32())<<32);
					var offset = reader.ReadInt32();
					story_offsets.Add(offset);

					Debug.Print(story_name.ToStringHash().Substring(0,8) + ": " + offset.ToString("X8"));
				}
				var stories_start = reader.BaseStream.Position;
				for(int i = 0; i < story_count; i++) { //TODO
					reader.BaseStream.Position = stories_start+story_offsets[i];
					reader.ReadInt32Check(val => {return val == 7;}, "7 is {0} when 7 is expected: " + stream.Position.ToString("X8"));
					var curve_count = reader.ReadInt32();
					reader.ReadInt32Check(val => {return val == 0;}, "0 is {0} when 0 is expected: " + stream.Position.ToString("X8"));
					reader.ReadInt32Check(val => {return val == 2;}, "2 is {0} when 2 is expected: " + stream.Position.ToString("X8"));
					reader.ReadInt32Check(val => {return val == 0;}, "0 is {0} when 0 is expected: " + stream.Position.ToString("X8"));
				}
				Debug.indent--;
				reader.BaseStream.Position = og_offset; */

				og_offset = reader.BaseStream.Position;
				reader.BaseStream.Position = section_adress_particles;
				Debug.Print("-Particles-");
				Debug.indent++;
				for(int i = 0; i < particles_count; i++) {
					Debug.Print("---");
					var effect = new MurmurHash2(reader.ReadInt64());
					var pos = reader.ReadVector3();
					var rot = reader.ReadQuaternion();
					var scl = reader.ReadVector3();
					var spawn = reader.ReadInt32() > 0;
					reader.ReadInt32Check(val => {return val == 0;}, "Particles UNKNOWN_ZERO is {0} when 0 is expected: " + stream.Position.ToString("X8"));

					#region PrintInfo
					Debug.Print("effect: " + effect.ToStringHash());
					Debug.Print("pos: " + pos);
					Debug.Print("rot: " + rot);
					Debug.Print("scl: " + scl);
					Debug.Print("spawn: " + spawn);
					#endregion
				}
				Debug.indent--;
				reader.BaseStream.Position = og_offset;

				ConverterUtils.ProcessSection(reader, "Units", _ProcessSectionUnits);
				ConverterUtils.ProcessSectionAtAdress(reader, "UnitData", _ProcessSectionUnitData, section_adress_unit_data);
				
				/* for(int i = 0; i < unitCount; i++) {
					reader.ReadInt32Check(val => {return val == 0;}, "UNKNOWN_ZERO3 is {0} when 0 is expected: " + stream.Position.ToString("X8"));
				}
				Debug.Print(stream.Position.ToString("X8")); */
				
				/* var level_setting_count = reader.ReadInt32();
				Debug.Print("level_setting_count: " + level_setting_count);
				Debug.Print("-Level Settings-");
				Debug.indent++;
				for(int i = 0; i < level_setting_count; i++) {
					var key_count = reader.ReadInt32();
					List<MurmurHash2> keys = new List<MurmurHash2>();
					for(int iii = 0; iii < key_count; iii++) {
						keys.Add(new MurmurHash2(((long)reader.ReadInt32())<<32));
					}
					var key_string = string.Concat(keys.Select(hash => {return hash.ToStringHash()+".";}));
					var type = reader.ReadInt32();
					switch(type) {
						case 3: //string
							var string_length = reader.ReadInt32();
							var chars = reader.ReadChars(string_length);
							Debug.Print(key_string + " = " + new string(chars));
							break;
						case 2: //number
							var single = reader.ReadSingle();
							Debug.Print(key_string + " = " + single);
							break;
						case 1: //bool
							bool boolean = reader.ReadInt32() > 0;
							Debug.Print(key_string + " = " + boolean);
							break;
					}
				}
				Debug.indent--; */

				Debug.Print(stream.Position.ToString("X8"));
			}
		}

		void _ProcessSectionUnits(BinaryReader reader) {
			units.Clear();
			Debug.Print("unitCount: " + unitCount);
			Debug.Print("-Units-");
			Debug.indent++;
			for(int i = 0; i < unitCount; i++) {
				Debug.Print("---");
				reader.ReadInt32Check(val => {return val == 0;}, "UNKNOWN_ZERO1 is {0} when 0 is expected: " + reader.BaseStream.Position.ToString("X8"));
				reader.ReadInt32Check(val => {return val == 0;}, "UNKNOWN_ZERO2 is {0} when 0 is expected: " + reader.BaseStream.Position.ToString("X8"));
				var unit = new LevelUnit();
				units.Add(unit);
				unit.id = new MurmurHash2(reader.ReadInt64());
				unit.name = new MurmurHash2(reader.ReadInt64());
				unit.type = new MurmurHash2(reader.ReadInt64());
				unit.material = new MurmurHash2(reader.ReadInt64());
				unit.pos = reader.ReadVector3();
				unit.rot = reader.ReadQuaternion();
				unit.scl = reader.ReadVector3();
				
				#region PrintInfo
				Debug.Print("id: " + unit.id.ToStringHash());
				Debug.Print("name: " + unit.name.ToStringHash());
				Debug.Print("type: " + unit.type.ToStringHash());
				Debug.Print("material: " + unit.material.ToStringHash());
				Debug.Print("pos: " + unit.pos);
				Debug.Print("rot: " + unit.rot);
				Debug.Print("scl: " + unit.scl);
				#endregion
			}
			Debug.indent--;
		}

		void _ProcessSectionUnitData(BinaryReader reader) {
			//unit.data
			List<int> data_adresses = new List<int>();
			for(int i = 0; i < unitCount; i++) {
				//reader.ReadInt64Check(val => {return val == 0;}, "UNKNOWN_ZERO is {0} when 0 is expected: " + stream.Position.ToString("X8"));
				data_adresses.Add(reader.ReadInt32()); //why? because stingray
			}

			Debug.Print("-Unit Data-");
			Debug.indent++;
			for(int i = 0; i < unitCount; i++) {
				var adress = data_adresses[i];
				Debug.Print("--- " + adress.ToString("X8"));
				if(adress > 0) {
					ReadSJSONData(reader, adress, units[i].data);
				}
			}
			Debug.indent--;
		}

		public byte[] ToBytes() {
			throw new System.NotImplementedException();
		}

		void ReadSJSONData(BinaryReader reader, int offset, Dictionary<string, dynamic> data_table) {
			var og_offset = reader.BaseStream.Position;
			reader.BaseStream.Position = offset;

			var entry_count = reader.ReadInt32();
			Debug.Print("entry_count: " + entry_count);
			Debug.indent++;
			for(int i = 0; i < entry_count; i++) {
				var key_count = reader.ReadInt32();
				List<MurmurHash2> keys = new List<MurmurHash2>();
				for(int iii = 0; iii < key_count; iii++) {
					keys.Add(new MurmurHash2(((long)reader.ReadInt32())<<32));
				}
				var key_string = string.Join('.', keys.Select(hash => {return hash.ToStringHash().Substring(0,8);}));

				var type = reader.ReadInt32();
				switch(type) {
					case 3: //string
						var string_length = reader.ReadInt32();
						var str = reader.ReadChars(string_length-1);
						reader.BaseStream.Position++;
						var padding = 4 - (string_length % 4);
						if(padding < 4) reader.BaseStream.Position += padding;
						Debug.Print("padding: " + padding);
						Debug.Print(key_string + " = " + new string(str));
						data_table.Add(key_string, new string(str));
						break;
					case 2: //number
						var single = reader.ReadSingle();
						Debug.Print(key_string + " = " + single);
						data_table.Add(key_string, single);
						break;
					case 1: //bool
						bool boolean = reader.ReadInt32() > 0;
						Debug.Print(key_string + " = " + boolean);
						data_table.Add(key_string, boolean);
						break;
				}
			}
			Debug.indent--;
			reader.BaseStream.Position = og_offset;
		}
	}
}