namespace VTExtract {
	public interface IConvertible {
		byte[] ToBytes();
		void FromBytes(byte[] data);
	}
}