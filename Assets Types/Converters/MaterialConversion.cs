
using System.IO;

namespace VTExtract {
	public partial class Material: IConvertible {
		public Material(string name) {
			this.name = name;
		}

		public void FromBytes(byte[] data) {
			using(var stream = new MemoryStream(data))
			using(var reader = new BinaryReader(stream)) {
				version = reader.ReadInt32Check(val => {return val == 43;}, "Level Version is {0} when 43 is expected: " + stream.Position.ToString("X8"));

				#region PrintInfo
				Debug.Print("Name: " + name);
				Debug.Print("Version: " + version);
				#endregion

				reader.ReadInt32Check(val => {return val == 1;}, "1 is {0} when 1 is expected: " + stream.Position.ToString("X8"));
				reader.ReadInt32Check(val => {return val == 24;}, "2 is {0} when 24 is expected: " + stream.Position.ToString("X8"));

				var UNKNOWN_parameters_data_size = reader.ReadInt32();
				Debug.Print("UNKNOWN_parameters_data_size: " + UNKNOWN_parameters_data_size);

				reader.ReadInt32Check(val => {return val == -1;}, "3 is {0} when -1 is expected: " + stream.Position.ToString("X8"));
				reader.ReadInt32Check(val => {return val == 0;}, "4 is {0} when 0 is expected: " + stream.Position.ToString("X8"));
				reader.ReadInt32Check(val => {return val == 0;}, "5 is {0} when 0 is expected: " + stream.Position.ToString("X8"));

				var parentMaterial = new MurmurHash2(reader.ReadInt64());

				var texture_count = reader.ReadInt32();
				Debug.Print("texture_count:" + texture_count);
				Debug.Print("-Textures-");
				Debug.indent++;
				for(int i = 0; i < texture_count; i++) {
					var tex_name = new MurmurHash2(((long)reader.ReadInt32())<<32);
					var tex_resource = new MurmurHash2(reader.ReadInt64());
					Debug.Print(tex_name.ToStringHash().Substring(0,8) + " = " + tex_resource.ToStringHash());
					textures.Add(tex_name, tex_resource);
				}
				Debug.indent--;

				var material_context_count = reader.ReadInt32();
				Debug.Print("material_context_count:" + material_context_count);
				Debug.Print("-Material Contexts-");
				Debug.indent++;
				for(int i = 0; i < material_context_count; i++) {
					var mc_name = new MurmurHash2(((long)reader.ReadInt32())<<32);
					var mc_resource = new MurmurHash2(((long)reader.ReadInt32())<<32);
					Debug.Print(mc_name.ToStringHash().Substring(0,8) + " = " + mc_resource.ToStringHash().Substring(0,8));
					materialContexts.Add(mc_name, mc_resource);
				}
				Debug.indent--;
			}
		}

		public byte[] ToBytes() {
			throw new System.NotImplementedException();
		}
	}
}