using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;
using Ionic.Zlib;
using System.Threading;

namespace VTExtract {
	public partial class Bundle:IConvertible {
		public byte[] ToBytes() {
			throw new System.NotImplementedException();
		}

		public void FromBytes(byte[] data) {
			var version = BitConverter.ToInt32(data, 0);
			Console.WriteLine("version: " + version.ToString("X8"));
			var unpacked_size = BitConverter.ToInt32(data, 4);
			Console.WriteLine("unpacked_size: " + unpacked_size);
			var unknown_zero = BitConverter.ToInt32(data, 8);
			Console.WriteLine("unknown_zero: " + unknown_zero);

			byte[] unpacked_data = new byte[unpacked_size];
			var codec = new ZlibCodec();
			codec.InputBuffer = data;
			codec.OutputBuffer = unpacked_data;
			codec.AvailableBytesOut = unpacked_data.Length;
			codec.NextIn = 4 * 3; //Jump 3 ints
			codec.NextOut = 0;

			while(codec.NextIn < data.Length) {
				var chunk_size = BitConverter.ToInt32(data, codec.NextIn);
				codec.NextIn += 4;

				//Debug.Print("codec.NextIn: " + codec.NextIn);
				//Debug.Print("chunk_size: " + chunk_size);
				//Debug.Print("codec.AvailableBytesOut: " + codec.AvailableBytesOut);

				if(chunk_size != 0x10000) {
					codec.AvailableBytesIn = chunk_size;

					codec.InitializeInflate();
					codec.Inflate(FlushType.None);
					codec.EndInflate();
				}
				else {
					codec.NextIn += chunk_size; //Skip those weird 65536 bytes big chunks
				}
			}

			BuildAssetList();
		}

		public void FromBytesMT(string path) {
			name = Path.GetFileNameWithoutExtension(path);
			extension = Path.GetExtension(path);
			filePath = path;
			
			List<byte[]?> chunks = new List<byte[]?>();

			using(FileStream stream = File.OpenRead(path))
			using(BinaryReader reader = new BinaryReader(stream)) {
				versionNumber = reader.ReadInt32();
				//Console.WriteLine("version: " + version.ToString("X8"));
				unpackedSize = reader.ReadInt32();
				//Console.WriteLine("unpacked_size: " + unpacked_size);
				var unknown_zero = reader.ReadInt32();
				//Console.WriteLine("unknown_zero: " + unknown_zero);

				if(!Console.IsOutputRedirected) Console.CursorVisible = false;
				var task = Task.Factory.StartNew(() => {
					while(stream.Position < stream.Length) {
						var chunk_size = reader.ReadInt32();

						if(chunk_size != 0x10000) {
							var data = reader.ReadBytes(chunk_size);
							chunks.Add(data);
						} else {
							chunks.Add(null);
							stream.Position += chunk_size;
						}
					}
				});
				while(!task.IsCompleted) {
					if(!Console.IsOutputRedirected) Console.CursorLeft = 0;
					Console.Write("Loading Chunks: {0}/{1} ({2:P2})", stream.Position, stream.Length, (float)stream.Position / stream.Length);
				}
				if(!Console.IsOutputRedirected) Console.CursorLeft = 0;
				Console.WriteLine("Loading Chunks: {0}/{1} ({2:P2}) DONE", stream.Position, stream.Length, (float)stream.Position / stream.Length);

				int progress = 0;
				byte[] new_unpacked_data = new byte[unpackedSize];
				task = Task.Factory.StartNew(() => {
					Parallel.For(0, chunks.Count, i => {
						byte[]? chunk = chunks[i];
						if(chunk != null) {
							var unpacked_chunk = ZlibStream.UncompressBuffer(chunk);
							//Debug.Print("unpacked_chunk: " + unpacked_chunk.Length);
							int pos = 0x10000 * i;
							int size = Math.Min(unpackedSize - pos, 0xFFFF);
							Array.Copy(unpacked_chunk, 0, new_unpacked_data, pos, size);
						} //else Console.WriteLine("chunk_size != 0x10000: " + 0x10000 * i);
						Interlocked.Increment(ref progress);
					});
				});
				do {
					if(!Console.IsOutputRedirected) Console.CursorLeft = 0;
					Console.Write("Processing Chunks: {0}/{1} ({2:P2})", progress, chunks.Count, (float)progress / chunks.Count);
				} while(!task.IsCompleted);
				if(!Console.IsOutputRedirected) Console.CursorLeft = 0;
				Console.WriteLine("Processing Chunks: {0}/{1} ({2:P2}) DONE", progress, chunks.Count, (float)progress / chunks.Count);

				unpackedData = new_unpacked_data;
			}
			if(!Console.IsOutputRedirected) Console.CursorVisible = true;

			BuildAssetList();

			/* Console.WriteLine("| {0,-16} | {1,-16} | {2,-8} | {3,-8} |", "Name", "Extension", "Position", "Size");
			string line = new string('-',18);
			string line_smol = new string('-',10);
			Console.WriteLine("|{0}|{0}|{1}|{1}|", line, line_smol);
			foreach(var asset in assets) {
				Console.WriteLine("| {0:X16} | {1:X16} | {2:X8} | {3:X8} |", asset.name, asset.extension, asset.position, asset.size);
			} */
		}

		public void BuildAssetList() {
			if(unpackedData == null) {
				Debug.PrintWarning("Tried to build the asset list without having unpacked the bundle");
				return;
			}
			var asset_count = BitConverter.ToInt32(unpackedData, 0);
			Console.WriteLine("asset_count: " + asset_count);

			//File.WriteAllBytes("./temp_output_unpacked_data", unpackedData);

			int assets_start_pos = 260 + 24 * asset_count;
			int total_size = 0;
			for(int i = 0; i < asset_count; i++) {
				int pos = 260 + 24 * i;
				var asset_extension = BitConverter.ToInt64(unpackedData, pos);
				var asset_name = BitConverter.ToInt64(unpackedData, pos + 8);
				var unknown_zero = BitConverter.ToInt32(unpackedData, pos + 16);
				var asset_size = BitConverter.ToInt32(unpackedData, pos + 20);
				var asset_position = assets_start_pos+total_size;
				var asset_header = new Byte[36];
				Array.Copy(unpackedData, asset_position, asset_header, 0, asset_header.Length);
				
				assets.Add(new Asset() {
					name = asset_name,
					extension = asset_extension,
					position = asset_position+36,
					size = asset_size,
					header = asset_header,
				});

				total_size += asset_size+36;
			}
			//Console.WriteLine("asset_count: " + asset_count);
		}

		public void DumpAllAssets() {
			if(name == null || extension == null || unpackedData == null) {
				Debug.PrintWarning("Tried to dump all asset without having unpacked the bundle");
				return;
			}
			string path = Path.Combine("./", name);
			Directory.CreateDirectory(path);
			int progress = 0;
			var task = Task.Factory.StartNew(() => {
				Parallel.ForEach(assets, asset => {
					byte[] data = new byte[asset.size];
					Array.Copy(unpackedData, asset.position, data, 0, data.Length);
					File.WriteAllBytes(Path.Combine(path, asset.GetNameWithExtension()), data);
					Interlocked.Increment(ref progress);
				});
			});
			do {
				if(!Console.IsOutputRedirected) Console.CursorLeft = 0;
				Console.Write("Dumping Assets: {0}/{1} ({2:P2})", progress, assets.Count, (float)progress / assets.Count);
			} while(!task.IsCompleted);
			if(!Console.IsOutputRedirected) Console.CursorLeft = 0;
			Console.WriteLine("Dumping Assets: {0}/{1} ({2:P2}) DONE", progress, assets.Count, (float)progress / assets.Count);
		}
	}
}