using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text.Json.Serialization;

namespace VTExtract {
	public partial class Level {
		public class LevelUnit {
			public MurmurHash2 id { get; set; }
			public MurmurHash2 name { get; set; }
			public MurmurHash2 type { get; set; }
			public MurmurHash2 material { get; set; }

			public Vector3 pos { get; set; }
			public Quaternion rot { get; set; }
			public Vector3 scl { get; set; }
			
			public Dictionary<string, dynamic> data { get; set; } = new Dictionary<string, dynamic>();
		}

		[JsonIgnore] public string name { get; private set; }
		[JsonIgnore] public int version { get; private set; }
		[JsonIgnore] public int unitCount { get; private set; }
		[JsonIgnore] public int backgroundUnitCount { get; private set; }

		public List<LevelUnit> units { get; private set; } = new List<LevelUnit>();
		[JsonPropertyName("level_settings")] public Dictionary<string, dynamic> levelSettings { get; private set; } = new Dictionary<string, dynamic>();
	}
}