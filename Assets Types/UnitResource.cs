using System;
using System.Collections.Generic;
using System.Numerics;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace VTExtract {
	public partial class UnitResource {
		public class Renderable {
			[Flags]
			public enum Flags { //TODO Check
				VIEWPORT_VISIBLE = 0b000000001, //Renderables that aren't VIEWPORT_VISIBLE are not included in units by default
				SHADOW_CASTER = 0b000000010,
				CULLING_DISABLED = 0b000000100,
				OCCLUDER = 0b000010000,
				SURFACE_QUERIES = 0b000100000,
				ALWAYS_KEEP = 0b100000000,
			}
			//public List<Vertex> vertices = new List<Vertex>();
			public int name; //TODO Change to hash
			public int nodeId;
			public int unknown_02;
			/// <summary>
			/// Start Value seems to be renderable count+1
			/// </summary>
			public int unknownSkinThing;
			public int flags;
			public Bounds bounds;
			public float unknown_06;
		}

		public class Light {
			public enum Type { omni, spot, box, directional }

			public int name; //TODO Change to hash
		}

		public class Node {
			public int name; //TODO Change to hash
			public Vector3 localXAxis;
			public Vector3 localYAxis;
			public Vector3 localZAxis;
			public Vector3 localPosition;
			public Vector3 localScale;
			public Matrix4x4 localPose;
			public Matrix4x4 worldPose;
			public short parentId;
			public short UNKNOWN_one;
			public bool isBone;

			public List<Node> children = new List<Node>();
		}

		public class Actor {
			public class Shape {
				public enum Type { sphere, box, capsule, mesh, convex }

				[JsonPropertyName("type")]
				public string typeProp {
					get { return type.ToString(); }
					set { type = Enum.Parse<Type>(value); }
				}
				public Type type;

				[JsonPropertyName("material")]
				public string materialProp {
					get { return material.ToString("X"); }
					set { material = int.Parse(value, System.Globalization.NumberStyles.HexNumber); }
				}
				public int material; //TODO CHange to hash

				[JsonPropertyName("template")]
				public string templateProp {
					get { return template.ToString("X"); }
					set { template = int.Parse(value, System.Globalization.NumberStyles.HexNumber); }
				}
				public int template; //TODO CHange to hash

				public Matrix4x4 pose;
				public int physx_data_size;

				[JsonPropertyName("shape")]
				public string shapeProp {
					get { return shape.ToString("X"); }
					set { shape = int.Parse(value, System.Globalization.NumberStyles.HexNumber); }
				}
				public int shape; //TODO CHange to hash

				public byte[]? physx_data;
			}

			[JsonPropertyName("name")]
			public string nameProp {
				get { return name.ToString("X"); }
				set { name = int.Parse(value, System.Globalization.NumberStyles.HexNumber); }
			}
			public int name; //TODO CHange to hash

			[JsonPropertyName("template")]
			public string templateProp {
				get { return template.ToString("X"); }
				set { template = int.Parse(value, System.Globalization.NumberStyles.HexNumber); }
			}
			public int template; //TODO CHange to hash

			[JsonPropertyName("node")]
			public string nodeProp {
				get { return node.ToString("X"); }
				set { node = int.Parse(value, System.Globalization.NumberStyles.HexNumber); }
			}
			public int node; //TODO Change to hash

			public float mass { get; set; }
			public List<Shape> shapes { get; set; } = new List<Shape>();
			public byte[]? FFx24;

			public bool enabled { get; set; }
		}

		public class Mover {
			[JsonPropertyName("name")]
			public string nameProp {
				get { return name.ToString("X"); }
				set { name = int.Parse(value, System.Globalization.NumberStyles.HexNumber); }
			}
			public int name;

			public float height { get; set; }
			public float radius { get; set; }

			[JsonPropertyName("collision_filter")]
			public string collisionFilterProp {
				get { return collisionFilter.ToString("X"); }
				set { collisionFilter = int.Parse(value, System.Globalization.NumberStyles.HexNumber); }
			}
			public int collisionFilter;

			public float slope_limit { get; set; }
			public byte[]? FFx8;
		}

		public class Mesh {
			public class GeoSection {
				public enum Type { POSITION, NORMAL, UNKNOWN_UV, UNKNOWN_UV2, TEXCOORD, COLOR, UNKNOWN_SKIN, UNKNOWN_SKIN2 }

				public byte[] data = new byte[0];
				public int UNKNOWN_ZERO;
				public int UNKNOWN_ZERO2;
				public int UNKNOWN_sub_id;
				public int elementCount;
				public int elementSize;
				public Type elementType;
				public int UNKNOWN;
				public byte UNKNOWN_ZERO_BYTE;
			}
			
			public class Material {
				public int id;
				public int triArrayStart;
				public int triCount;
				public int unknown_zero;
				public int name;
			}

			public List<GeoSection> sections = new List<GeoSection>();
			public List<int> triangles = new List<int>();
			public List<Material> materials = new List<Material>();
		}

		public class Skin {
			public List<int> boneIds = new List<int>();
		}

		public string name { get; private set; }
		public int version { get; private set; }
		//public int nodeCount { get; private set; }

		public List<Renderable> renderables = new List<Renderable>();
		public List<Mesh> meshes = new List<Mesh>();
		public List<Node> nodes = new List<Node>();
		public List<Actor> actors = new List<Actor>();
		public List<Mover> movers = new List<Mover>();
		public List<Light> lights = new List<Light>();
		public List<Skin> skins = new List<Skin>();
	}
}