
using System;
using System.IO;
using System.Numerics;

namespace VTExtract {
	public static class BinaryReaderExtension {
		public static Matrix4x4 ReadMatrix4x4(this BinaryReader reader) {
			Matrix4x4 matrix = new Matrix4x4();
			matrix.M11 = reader.ReadSingle();
			matrix.M12 = reader.ReadSingle();
			matrix.M13 = reader.ReadSingle();
			matrix.M14 = reader.ReadSingle();

			matrix.M21 = reader.ReadSingle();
			matrix.M22 = reader.ReadSingle();
			matrix.M23 = reader.ReadSingle();
			matrix.M24 = reader.ReadSingle();

			matrix.M31 = reader.ReadSingle();
			matrix.M32 = reader.ReadSingle();
			matrix.M33 = reader.ReadSingle();
			matrix.M34 = reader.ReadSingle();

			matrix.M41 = reader.ReadSingle();
			matrix.M42 = reader.ReadSingle();
			matrix.M43 = reader.ReadSingle();
			matrix.M44 = reader.ReadSingle();
			return matrix;
		}
		
		public static Bounds ReadBounds(this BinaryReader reader) {
			Bounds bounds = new Bounds();
			bounds.lx = reader.ReadSingle();
			bounds.ly = reader.ReadSingle();
			bounds.lz = reader.ReadSingle();

			bounds.ux = reader.ReadSingle();
			bounds.uy = reader.ReadSingle();
			bounds.uz = reader.ReadSingle();
			bounds.UNKNOWN = reader.ReadSingle();
			return bounds;
		}
				
		public static Vector3 ReadVector3(this BinaryReader reader) {
			Vector3 vector = new Vector3();
			vector.X = reader.ReadSingle();
			vector.Y = reader.ReadSingle();
			vector.Z = reader.ReadSingle();
			return vector;
		}

		public static Vector2 ReadVector2(this BinaryReader reader) {
			Vector2 vector = new Vector2();
			vector.X = reader.ReadSingle();
			vector.Y = reader.ReadSingle();
			return vector;
		}

		public static Quaternion ReadQuaternion(this BinaryReader reader) {
			Quaternion quaternion = new Quaternion();
			quaternion.X = reader.ReadSingle();
			quaternion.Y = reader.ReadSingle();
			quaternion.Z = reader.ReadSingle();
			quaternion.W = reader.ReadSingle();
			return quaternion;
		}

		public static Int64 ReadInt64Check(this BinaryReader reader, Func<Int64, bool> check_func, string fail_message) {
			var val = reader.ReadInt64();
			bool passed = check_func(val);
			if(!passed) Debug.PrintWarning(string.Format(fail_message, val));
			return val;
		}

		public static Int32 ReadInt32Check(this BinaryReader reader, Func<Int32, bool> check_func, string fail_message) {
			var val = reader.ReadInt32();
			bool passed = check_func(val);
			if(!passed) Debug.PrintWarning(string.Format(fail_message, val));
			return val;
		}

		public static Int16 ReadInt16Check(this BinaryReader reader, Func<Int16, bool> check_func, string fail_message) {
			var val = reader.ReadInt16();
			bool passed = check_func(val);
			if(!passed) Debug.PrintWarning(string.Format(fail_message, val));
			return val;
		}

		public static Byte ReadByteCheck(this BinaryReader reader, Func<Byte, bool> check_func, string fail_message) {
			var val = reader.ReadByte();
			bool passed = check_func(val);
			if(!passed) Debug.PrintWarning(string.Format(fail_message, val));
			return val;
		}

		public static float ReadSingleCheck(this BinaryReader reader, Func<float, bool> check_func, string fail_message) {
			var val = reader.ReadSingle();
			bool passed = check_func(val);
			if(!passed) Debug.PrintWarning(string.Format(fail_message, val));
			return val;
		}
	}
}