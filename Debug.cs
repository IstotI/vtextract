using System;
using System.IO;
using System.Runtime.CompilerServices;

namespace VTExtract {
	public static class Debug {
		public static int level = 2;
		public static int indent = 0;

		public enum ErrorCode {
			FILE_NOT_FOUND = 404,
			DATA_NOT_FOUND = 405,
			INVALID_DATA = 300,
			DATA_NOT_SUPPORTED = 301,
			OTHER = 666,
		}
		
		static void _Print(object message, string caller_path, int? line_number) {
			var file_name = Path.GetFileNameWithoutExtension(caller_path);
			Console.WriteLine("[{0}:{1}] {2}{3}", file_name, line_number, new string(' ', indent*2), message);
		}
		
		//TODO Add formated version
		public static void Print(object message, [CallerFilePath] string caller_path = "", [CallerLineNumber] int? line_number = null) {
			if(level < 3) return;

			_Print(message, caller_path, line_number);
		}

		public static void PrintError(object message, ErrorCode error_code, [CallerFilePath] string caller_path = "", [CallerLineNumber] int? line_number = null) {
			if(level < 1) return;

			var old_fg_color = Console.ForegroundColor;
			Console.ForegroundColor = ConsoleColor.Red;

			_Print("//ERROR// " + message, caller_path, line_number);

			Console.ForegroundColor = old_fg_color;
			Environment.Exit((int)error_code);
		}

		public static void PrintWarning(object message, [CallerFilePath] string caller_path = "", [CallerLineNumber] int? line_number = null) {
			if(level < 2) return;

			var old_fg_color = Console.ForegroundColor;
			Console.ForegroundColor = ConsoleColor.Yellow;

			_Print("//WARNING// " + message, caller_path, line_number);

			Console.ForegroundColor = old_fg_color;
		}
	}
}