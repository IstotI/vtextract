using System;
using System.Text;

public class MurmurHash2 {
	const ulong m = 0xc6a4a7935bd1e995;
	const ulong minv = 0x5f7a0ea7e59b19bd;
	const int r = 47;
		
	/// <summary>
	/// The hash generated from a string or used to generate an inverse hash
	/// </summary>
	public long hash { get; private set; }

	/// <summary>
	/// The original string used to generate the hash
	/// </summary>
	/// <remarks>
	/// This value is null when generating a inverse hash
	/// </remarks>
	public string? originalString { get; private set; }

	/// <summary>
	/// The hash that would result in the current hash if used as input
	/// </summary>
	public long inverseHash { get; private set; }

	public MurmurHash2(string input) {
		originalString = input;

		hash = GetHash(input);
		inverseHash = GetInverseHash(hash);
	}

	public MurmurHash2(byte[] input) {
		hash = GetHash(input);
		inverseHash = GetInverseHash(hash);
	}

	public MurmurHash2(long hash) {
		//originalString = input;
		this.hash = hash;
		inverseHash = GetInverseHash(hash);
	}

	public static long GetHash(byte[] input) {
		var b_length = input.Length;
		ulong hash = 0 ^ ((ulong)b_length * m);
		int longs_count = b_length / 8;
		int remainder = b_length % 8;
		int remainder_pos = longs_count * 8;

		for(int i = 0; i < longs_count; i++) {
			var v = BitConverter.ToUInt64(input, i*8);

			v *= m;
			v ^= v >> r;
			v *= m;

			hash ^= v;
			hash *= m;
		}

		if(remainder > 0) {
			for(int i = remainder; i > 0; i--) {
				int i2 = i - 1;
				hash ^= (ulong)input[remainder_pos + i2] << (8 * i2);
			}
			hash *= m;
		}

		hash ^= hash >> r;
		hash *= m;
		hash ^= hash >> r;

		return (long)hash;
	}

	public static long GetHash(string input) {
		var bytes = Encoding.UTF8.GetBytes(input);
		return GetHash(bytes);
		/* var b_length = bytes.Length;
		ulong hash = 0 ^ ((ulong)b_length * m);
		int longs_count = b_length / 8;
		int remainder = b_length % 8;
		int remainder_pos = longs_count * 8;

		for(int i = 0; i < longs_count; i++) {
			var v = BitConverter.ToUInt64(bytes, i*8);

			v *= m;
			v ^= v >> r;
			v *= m;

			hash ^= v;
			hash *= m;
		}

		if(remainder > 0) {
			for(int i = remainder; i > 0; i--) {
				int i2 = i - 1;
				hash ^= (ulong)bytes[remainder_pos + i2] << (8 * i2);
			}
			hash *= m;
		}

		hash ^= hash >> r;
		hash *= m;
		hash ^= hash >> r;

		return (long)hash; */
	}

	public static long GetInverseHash(string input) {
		return GetInverseHash(666); //TODO
	}

	//https://web.archive.org/web/20191127044648/https://bitsquid.blogspot.com/2011/08/code-snippet-murmur-hash-inverse-pre.html
	public static long GetInverseHash(long input) {
		ulong hash = (ulong)input;

		hash ^= hash >> r;
		hash *= minv;
		hash ^= hash >> r;
		hash *= minv;

		ulong h = 0 ^ unchecked((ulong)8 * m);
		hash ^= h;
		
		hash *= minv;
		hash ^= hash >> r;
		hash *= minv;

		var bytes = BitConverter.GetBytes(hash);
		byte[] fin_bytes = new byte[bytes.Length];
		for(int i = bytes.Length-1; i >= 0; i--) {
			fin_bytes[bytes.Length-1-i] = bytes[i];
		}

		return BitConverter.ToInt64(fin_bytes);//(long)hash;
	}

	public string ToStringHash() {
		return hash.ToString("X16");
	}

	public string ToStringInverseHash() {
		return inverseHash.ToString("X16");
	}

	public string ToStringInverseHashUTF8() {
		var bytes = BitConverter.GetBytes(inverseHash);
		return Encoding.UTF8.GetString(bytes);
	}
}