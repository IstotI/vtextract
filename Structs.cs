using System;
using System.Collections.Generic;

namespace VTExtract {
	/* public struct Vector3 {
		public float x;
		public float y;
		public float z;
	} */

	/* public struct Quaternion {
		public float x;
		public float y;
		public float z;
		public float w;
	} */

	public struct HalfFloat {
		//private short _bits;
		private bool _signBit;
		private byte _exponent;
		private short _mantissa;

		public HalfFloat(short data) {
			//_bits = data;
			_signBit = ((data & 0b1000000000000000) >> 15) != 0;
			_exponent = (byte)((data & 0b0111110000000000) >> 10);
			_mantissa = (short)(data & 0b0000001111111111);
		}

		public float ToFloat() {
			var rexp = (float)Math.Pow(2, _exponent - 15);
			var mant = Math.Abs(_mantissa / (float)0b10000000000);
			byte one = (byte)((_exponent > 0) ? 1 : 0);

			return (_signBit ? -1 : 1) * rexp * (one + mant);
		}

		public void PrintBinary() {
			//string s = Convert.ToString(_bits, 2).PadLeft(16,'0');
			var og_fg_color = Console.ForegroundColor;

			Console.ForegroundColor = ConsoleColor.Blue;
			Console.Write(_signBit ? 1 : 0);//s.Substring(0,1));
			Console.ForegroundColor = ConsoleColor.Green;
			Console.Write(Convert.ToString(_exponent, 2).PadLeft(5, '0'));//s.Substring(1,5));
			Console.ForegroundColor = ConsoleColor.Red;
			Console.Write(Convert.ToString(_mantissa, 2).PadLeft(10, '0'));//s.Substring(6,10));
			Console.ForegroundColor = og_fg_color;
		}
	}

	public struct Vector3Half {
		public HalfFloat x;
		public HalfFloat y;
		public HalfFloat z;
		public HalfFloat w;

		public Vector3Half(ulong data) {
			short first = (short)((data & 0xFFFF_0000_0000_0000) >> 48);
			short second = (short)((data & 0x0000_FFFF_0000_0000) >> 32);
			short third = (short)((data & 0x0000_0000_FFFF_0000) >> 16);
			short fourth = (short)(data & 0x0000_0000_0000_FFFF);
			//Console.WriteLine("{0:X4}, {1}", data, Convert.ToString((long)data, 2).PadLeft(64,'0'));
			//Console.WriteLine("{0:X4}, {1}", data, Convert.ToString((long)second, 2).PadLeft(16,'0'));

			//if(first != 0x3C00) Debug.Print("Got abnormal first value: " + first.ToString("X4") + "; 3C00 expected.");

			x = new HalfFloat(fourth);
			y = new HalfFloat(third);
			z = new HalfFloat(second);
			w = new HalfFloat(first);

			//Console.Write("{0:X4}, ", fourth); x.PrintBinary(); Console.Write(" = "); Console.Write(x.ToFloat()); Console.WriteLine();
			//Console.Write("{0:X4}, ", third); y.PrintBinary(); Console.Write(" = "); Console.Write(y.ToFloat()); Console.WriteLine();
			//Console.Write("{0:X4}, ", second); z.PrintBinary(); Console.Write(" = "); Console.Write(z.ToFloat()); Console.WriteLine();
		}

		public void Print(bool full_binary) {
			if(full_binary) {
				Console.WriteLine("Vector3Half");
				Console.Write("  "); x.PrintBinary(); Console.Write(" = "); Console.Write(x.ToFloat()); Console.WriteLine();
				Console.Write("  "); y.PrintBinary(); Console.Write(" = "); Console.Write(y.ToFloat()); Console.WriteLine();
				Console.Write("  "); z.PrintBinary(); Console.Write(" = "); Console.Write(z.ToFloat()); Console.WriteLine();
				Console.Write("  "); w.PrintBinary(); Console.Write(" = "); Console.Write(w.ToFloat()); Console.WriteLine();
			} else {
				Console.WriteLine("Vector3Half:{{{0}, {1}, {2}, {3}}}", x.ToFloat(), y.ToFloat(), z.ToFloat(), w.ToFloat());
			}
		}
	}

	public struct Vertex {
		public Vector3Half position;
		public Vector3Half normal;
		public Vector3Half weirdUV;
		public Vector3Half weirdUV2;
		public Vector3Half UV;
	}

	public struct Bounds {
		public float lx;
		public float ly;
		public float lz;
		public float ux;
		public float uy;
		public float uz;
		public float UNKNOWN;
	}

	/* public class UnitGeoChannel {
		private List<object> _elements;
		public Type type { get; private set; }

	}

	public struct UnitMeshGeometry {
		public List<UnitGeoChannel> channels;
	} */
}