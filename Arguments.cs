using System;
using System.Collections.Generic;
using System.Linq;

namespace VTExtract {
	public class Arguments {
		Dictionary<string, string> _args = new Dictionary<string, string>();
		public bool hasInvalidArguments { get; private set; } = false;
		
		public Arguments(string[] args, params string[] check) {
			List<string> args_list = args.ToList();

			foreach(var s in check) {
				int index = args_list.IndexOf(s);
				if(index >= 0) {
					if(index + 1 < args_list.Count && !check.Contains(args_list[index + 1])) {
						hasInvalidArguments = true;
						_args.Add(s, args_list[index + 1]);
						args_list.RemoveRange(index, 2);
					} else {
						hasInvalidArguments = true;
						Debug.PrintWarning("Argument: " + s + " is missing a value.");
						args_list.RemoveAt(index);
					}
				}
			}

			if(args_list.Count > 0) {
				hasInvalidArguments = true;
				foreach(var arg in args_list) {
					Debug.PrintWarning("Unknown Argument: " + arg);
				}
			}
		}

		public string? GetArgumentValue(string name) {
			string? value;
			_args.TryGetValue(name, out value);
			return value;
		}
	}
}