using System;
using System.Numerics;
using System.Text.Json;
using System.Text.Json.Serialization;

public class Vector2JsonConverter: JsonConverter<Vector2> {
	public override Vector2 Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) {
		throw new NotImplementedException();
	}

	public override void Write(Utf8JsonWriter writer, Vector2 value, JsonSerializerOptions options) {
		writer.WriteStartArray();
		writer.WriteNumberValue(value.X);
		writer.WriteNumberValue(value.Y);
		writer.WriteEndArray();
	}
}