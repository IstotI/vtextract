using System;
using System.Text.Json;
using System.Text.Json.Serialization;

public class MurmurHash2JsonConverter: JsonConverter<MurmurHash2> {
	public override MurmurHash2 Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) {
		long hash = long.Parse(reader.GetString(), System.Globalization.NumberStyles.HexNumber);
		return new MurmurHash2(hash);
	}

	public override void Write(Utf8JsonWriter writer, MurmurHash2 value, JsonSerializerOptions options) {
		writer.WriteStringValue(value.ToStringHash());
	}
}