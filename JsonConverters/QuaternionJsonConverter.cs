using System;
using System.Numerics;
using System.Text.Json;
using System.Text.Json.Serialization;

public class QuaternionJsonConverter: JsonConverter<Quaternion> {
	public override Quaternion Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) {
		throw new NotImplementedException();
	}

	public override void Write(Utf8JsonWriter writer, Quaternion value, JsonSerializerOptions options) {
		writer.WriteStartArray();
		writer.WriteNumberValue(value.X);
		writer.WriteNumberValue(value.Y);
		writer.WriteNumberValue(value.Z);
		writer.WriteNumberValue(value.W);
		writer.WriteEndArray();
	}
}